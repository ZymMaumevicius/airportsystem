const gulp = require('gulp');
const gutil = require('gulp-util');
const path = require('path');
const readline = require('readline');

const GULP_BIN = path.join(__dirname, 'node_modules', 'gulp', 'bin', 'gulp.js');

gulp.task('dev', () => {
	startEnvironment('backend', __dirname + '/server');
	startEnvironment('frontend', __dirname + '/site');

	function startEnvironment(name, dir) {
		const childProcess = require('child_process');
		// spawning a child process that runs the `gulp dev` task for a particular sub-project
		const environment = childProcess.spawn(
			'node',
			[GULP_BIN, 'watch', '--colors'].concat(process.argv.slice(3)),
			{ cwd: dir }
		);

		const outLineReader = readline.createInterface({ input: environment.stdout });
		const errLineReader = readline.createInterface({ input: environment.stderr });

		// if the main process (terminal) receives an input `rs` or similar, it will pass it down
		// to the child process
		process.stdin.on('data', (data) => {
			environment.stdin.write(data);
		});

		// reading the stdout stream line-by-line
		outLineReader.on('line', (line) => printLine(name, line));
		errLineReader.on('line', (line) => printLine(name, line));
	}

});

gulp.task('migrate-database:dev', () => {
	migrateDatabase('dev');

	function migrateDatabase(env) {
		const configPath = require(path.join(__dirname, 'config', `${env}.json`));
		const dir = __dirname + '/data/migrations';
		const migrator = path.join('..','..','node_modules', 'pg-migrator', 'bin', 'pg-migrator');
		const childProcess = require('child_process');

		const environment = childProcess.spawn(
			'node',
			[migrator, configPath.connectionString].concat(process.argv.slice(3)),
			{ cwd: dir }
		);

		const outLineReader = readline.createInterface({ input: environment.stdout });
		const errLineReader = readline.createInterface({ input: environment.stderr });

		// if the main process (terminal) receives an input `rs` or similar, it will pass it down
		// to the child process
		process.stdin.on('data', (data) => {
			environment.stdin.write(data);
		});

		// reading the stdout stream line-by-line
		outLineReader.on('line', (line) => printLine(env, line));
		errLineReader.on('line', (line) => printLine(env, line));
	}
});

function printLine(prefix, line) {
	gutil.log(
		gutil.colors.yellow(`${prefix}:`),
		line.replace(/^\[.+?\]\s/, '')
	);
}