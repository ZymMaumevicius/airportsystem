const gulp = require('gulp');
const nodemon = require('gulp-nodemon');
const babel = require('gulp-babel');
const Cache = require('gulp-file-cache');
const runSequence = require('run-sequence');
const del = require('del');

const cache = new Cache();

const babelConfig = {
	presets: ['es2015'],
	plugins: [
		['module-resolver', {
			alias: {
				'config': '../config',
				'core': './api/core'
			}
		}]
	]
};

gulp.task('clean', (cb) => {
	del(['./server/dist/**'])
		.then(() => cb());
});

gulp.task('pre-compile', ['copy-public'], () => {
	return gulp.src('./src/**/*.js')
		.pipe(babel(babelConfig))
		.pipe(gulp.dest('./dist'));
});

gulp.task('copy-public', function () {
	return gulp.src('./src/public/**/*')
		.pipe(gulp.dest('./dist/public'));
});

gulp.task('compile', function () {
	return gulp.src('./src/**/*.js') // your ES2015 code
		.pipe(cache.filter()) // remember files
		.pipe(babel(babelConfig)) // compile new ones
		.pipe(cache.cache()) // cache them
		.pipe(gulp.dest('./dist'));  // write them
});

gulp.task('watch', ['build'], function () {
	return nodemon({
		exec: 'node --inspect=9816',
		script: './dist/' // run ES5 code
		, watch: './src' // watch ES2015 code
		, tasks: ['compile'] // compile synchronously onChange
	});
});

gulp.task('build', (done) => {
	runSequence(
		'clean',
		'pre-compile',
		'copy-public',
		done
	);
});