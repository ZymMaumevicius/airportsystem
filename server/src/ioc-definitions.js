import knex from 'knex';
import AuthRepository from './api/repositories/AuthRepository';

import SessionFactory from './core/session/sessionFactory';

import AuthenticationProvider from './api/services/authentication/AuthenticationProvider';

import ValidationProvider from './api/services/validators/ValidationProvider';

import RoutesRepository from './api/repositories/RoutesRepository';
import FlightRepository from './api/repositories/FlightRepository';
import PlanesRepository from './api/repositories/PlanesRepository';


const db = knex({
	client: 'pg',
	connection: require('config/dev.json').connectionString,
	searchPath: 'knex,public'
});

let initDb = null;

const iocDefinitions = {
	'db': () => {
		if (!initDb) {
			initDb = db;
		}

		return initDb;
	},
	'authRepository': (container) => new AuthRepository(container.get('db')),
	'authenticationProvider': (container) => new AuthenticationProvider(container.get('authRepository'), container.get('session')),
	'session': () => new SessionFactory(),
	'routesRepository': (container) => new RoutesRepository(container.get('db')),
	'validationProvider': () => new ValidationProvider(),
	'flightRepository': (container) => new FlightRepository(container.get('db')),
	'planesRepository': (container) => new PlanesRepository(container.get('db'))
};

export default iocDefinitions;
