import when from 'when';
import path from 'path';
import iocDefinitions from '../ioc-definitions';
import Container from './ioc/ioc-container';

/**@type Container*/
const container = new Container(iocDefinitions);

/**@type AuthenticationProvider*/
const authProvider = container.get('authenticationProvider');

export function resolveRoute(route, req, reply, headerChange) {
	const splittedRoute = route.split('#');
	const controllersPath = path.join(__dirname, '..', 'api', 'controllers');
	const controller = require(path.join(controllersPath, splittedRoute[0] + 'Controller'));
	const iniciatedController = new controller.default(req, container);

	return when
		.try(() => iniciatedController[splittedRoute[1]]())
		.then((res) => {
			if (headerChange) {
				return reply(res.data)
					.header(headerChange, res.header);
			}

			return reply(res);
		})
		.catch((error) => {
			console.log('error', error);
			reply({
				error
			}).code(400);
		});
}

export function validateToken(decodedToken, request, callback) {
	authProvider.validateUser(decodedToken, request)
		.then((response) => {
			callback(null, !!response);
		});
}