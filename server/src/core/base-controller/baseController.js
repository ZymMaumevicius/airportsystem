export default class BaseController {
	constructor(request, container) {
		this.request = request;
		this.container = container;
	}
}
