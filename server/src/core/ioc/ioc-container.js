export default class Container {
	constructor(definitions) {
		this.definitions = definitions;
	}

	get(type) {
		return this.definitions[type](this);
	}


}