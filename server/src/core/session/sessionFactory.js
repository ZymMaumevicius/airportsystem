export default class SessionFactory {
	constructor() {
		this._currentUser = {};
	}

	getCurrentUser() {
		return this._currentUser;
	}

	setCurrentUser(user) {
		this._currentUser = user;
	}

}