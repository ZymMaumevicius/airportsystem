import { resolveRoute } from '../core/routeHandler';

const routes = [{
	method: 'GET',
	path: '/test',
	handler: (req, rep) => resolveRoute('Main#index', req, rep)
}, {
	method: 'POST',
	path: '/login',
	config: { auth: false },
	handler: (req, rep) => resolveRoute('Authorization#login', req, rep)
}, {
	method: 'POST',
	path: '/register',
	config: { auth: false },
	handler: (req, rep) => resolveRoute('Authorization#register', req, rep)
}, {
	method: 'GET',
	path: '/routes/getInfo',
	handler: (req, rep) => resolveRoute('Routes#getInfo', req, rep)
}, {
	method: 'POST',
	path: '/routes/save',
	handler: (req, rep) => resolveRoute('Routes#saveRoute', req, rep)
}, {
	method: 'GET',
	path: '/routes/info',
	handler: (req, rep) => resolveRoute('Routes#getFlightInfo', req, rep)
}, {
	method: 'POST',
	path: '/flights/saveFlight',
	handler: (req, rep) => resolveRoute('Flights#saveFlight', req, rep)
}, {
	method: 'GET',
	path: '/flights/getall',
	handler: (req, rep) => resolveRoute('Flights#getAll', req, rep)
}, {
	method: 'POST',
	path: '/planes/savePlane',
	handler: (req, rep) => resolveRoute('Planes#savePlane', req, rep)
}, {
    method: 'GET',
    path: '/planes/getInfo/{id}',
    handler: (req, rep) => resolveRoute('Planes#getPlaneInfo', req, rep)
}, {
	method: 'GET',
	path: '/planes/planelist',
	handler: (req, rep) => resolveRoute('Planes#getAllPlanes', req, rep)
}, {
	method: 'GET',
	path: '/planes/baggagelist',
	handler: (req, rep) => resolveRoute('Planes#getAllBaggage', req, rep)
}, {
	method: 'POST',
	path: '/flights/getbyid',
	handler: (req, rep) => resolveRoute('Flights#getById', req,rep)
},{
	method: 'GET',
	path: '/flights/assignable',
	handler: (req, rep) => resolveRoute('Flights#getAssignable', req, rep)
}, {
	method: 'POST',
	path: '/flights/assign',
	handler: (req, rep) => resolveRoute('Flights#assign', req, rep)
}, {
	method: 'GET',
	path: '/flights/status',
	handler: (req, rep) => resolveRoute('Flights#status', req, rep)
}, {
	method: 'GET',
	path: '/planes/baggage/edit/{id}',
	handler: (req, rep) => resolveRoute('Planes#getBaggage', req, rep)
}, {
	method: 'POST',
	path: '/planes/baggage/update',
	handler: (req, rep) => resolveRoute('Planes#updateBaggage', req, rep)
}, {
	method: 'GET',
	path: '/planes/tech/list',
	handler: (req, rep) => resolveRoute('Planes#getAllPlanesWithTech', req, rep)
}, {
	method: 'POST',
	path: '/planes/tech/save',
	handler: (req, rep) => resolveRoute('Planes#saveTechInfo', req, rep)
}, {
	method: 'GET',
	path: '/planes/crew/list',
	handler: (req, rep) => resolveRoute('Planes#getCrewList', req, rep)
}, {
	method: 'GET',
	path: '/planes/crew/info/{id}',
	handler: (req, rep) => resolveRoute('Planes#getCrewInfo', req, rep)
}, {
	method: 'GET',
	path: '/planes/crew/workers',
	handler: (req, rep) => resolveRoute('Planes#getWorkers', req, rep)
}, {
	method: 'GET',
	path: '/planes/crew/members/{id}',
	handler: (req, rep) => resolveRoute('Planes#getCrewMembers', req, rep)
}, {
	method: 'POST',
	path: '/planes/crew/save',
	handler: (req, rep) => resolveRoute('Planes#saveCrew', req, rep)
}, {
    method: 'GET',
    path: '/planes/crew/delete/{id}',
    handler: (req, rep) => resolveRoute('Planes#deleteCrew', req, rep)
}

];

exports.routes = (server) => server.route(routes.map((route) => {
	route.path = '/api' + route.path;

	return route;
}));
