import { Server } from 'hapi';
import path from 'path';
import {validateToken} from './core/routeHandler';

const server = new Server();

server.connection({
	port: 3000, host: 'localhost'
});

server.register([{
	register: require('hapi-routes'),
	options: {
		dir: `${__dirname}/routes`
	}
}, {
	register: require('inert')
}, {
	register: require('vision')
}, {
	register: require('hapi-auth-jwt2')
}], (err) => {
	if (err) {
		throw err;
	}

	server.auth.strategy('jwt', 'jwt',
		{
			key: require('config/dev.json').auth.key,
			validateFunc: validateToken,
			verifyOptions: { algorithms: ['HS256'] }
		});

	server.auth.default('jwt');

	server.route({
		method: 'GET',
		path: '/public/{param*}',
		config: { auth: false },
		handler: {
			directory: {
				path: path.join(__dirname, 'public'),
				listing: true
			}
		}
	});

	server.views({
		engines: {
			html: require('handlebars')
		},
		relativeTo: __dirname,
		path: 'public'
	});

	server.route({
		method: 'GET',
		path: '/{param*}',
		config: { auth: false },
		handler: function (request, reply) {
			reply.view('index.html');
		}
	});

	server.start(function (error) {

		if (error) {
			throw error;
		}

		console.log('Server running at: ', server.info.uri);// eslint-disable-line no-console
	});
});



