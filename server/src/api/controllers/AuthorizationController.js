import BaseController from '../../core/base-controller/baseController';

export default class AuthorizationController extends BaseController {
	constructor(...args) {
		super(...args);

		/**
		 * @type AuthenticationProvider
		 */
		this.authenticationProvider = this.container.get('authenticationProvider');
	}

	login() {
		const {email, password} = this.request.payload;

		return this.authenticationProvider.signIn(email, password)
			.then((result) => ({
					header: result
			}));
	}

	register() {
		const user = this.request.payload;
		return this.authenticationProvider.register(user);
	}
}