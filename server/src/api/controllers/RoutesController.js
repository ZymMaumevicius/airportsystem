import BaseController from '../../core/base-controller/baseController';

export default class MainController extends BaseController {
	constructor(...args) {
		super(...args);

		/**
		 * @type RoutesRepository
		 */
		this.routesRepository = this.container.get('routesRepository');
		/**
		 * @type ValidationProvider
		 */
		this.validator = this.container.get('validationProvider');
	}

	getInfo() {
		return this.routesRepository.getAirports();
	}

	saveRoute() {
		const { route } = this.request.payload;
		return this.validator.validate(route, ['airport_from_id',
			'airport_to_id',
			'valid_from',
			'valid_to',
			'planned_traffic',
			'fuel_consumption_coef',
			'distance',
			'airtime_duration'])
			.then((routes) => this.routesRepository.saveRoute(routes))
			.then(() => this.routesRepository.getFlightInfo());
	}

	getFlightInfo() {
		return this.routesRepository.getFlightInfo();
	}
}

