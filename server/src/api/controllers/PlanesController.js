import BaseController from '../../core/base-controller/baseController';

export default class PlanesController extends BaseController {
    constructor(...args) {
        super(...args);

        /**
         * @type PlanesRepository
         */
        this.planesRepository = this.container.get('planesRepository');

        /**
         * @type ValidationProvider
         */
        this.validator = this.container.get('validationProvider');
    }

    savePlane() {
        const {plane} = this.request.payload;
        return this.validator.validate(plane, [
            'number',
            'model',
            'make',
            'weight',
            'lifting_power',
            'baggage_volume',
            'business_seats',
            'economy_seats',
            'pilots',
            'stewards',
            'toilets'])
            .then((planes) => this.planesRepository.savePlane(planes))
            .then(() => this.planesRepository.getAllPlanes());
    }

    getAllPlanes() {
        return this.planesRepository.getAllPlanes();
    }

    getPlaneInfo() {
        return this.planesRepository.getPlaneInfo(this.request.params.id);
    }

    getAllBaggage() {
        return this.planesRepository.getAllBaggage();
    }

    getBaggage() {
        return this.planesRepository.getBaggage(this.request.params.id);
    }

    updateBaggage() {
        const {baggage} = this.request.payload;
        return this.validator.validate(baggage, [
            'id',
            'number',
            'plane_id',
            'weight',
            'width',
            'height',
            'length'])
            .then((bagg) => this.planesRepository.updateBaggage(bagg))
            .then(() => this.planesRepository.getAllBaggage());
    }

    getAllPlanesWithTech() {
        return this.planesRepository.getAllPlanesWithTech();
    }

    saveTechInfo() {
        const {tech} = this.request.payload;
        return this.validator.validate(tech, [
            'number',
            'date',
            'check_is_passed',
            'comments',
            'chassis',
            'flaps',
            'steering_mechanism',
            'landing_gear',
            'engines',
            'fuel_system',
            'communications',
            'lights',
            'brakes',
            'plane_id'])
            .then((techInfo) => this.planesRepository.saveTechInfo(techInfo))
            .then(() => this.planesRepository.getAllPlanesWithTech());
    }

    getCrewList() {
        return this.planesRepository.getCrewList();
    }

    getWorkers() {
        return this.planesRepository.getWorkers();
    }

    getCrewInfo() {
        return this.planesRepository.getCrewInfo(this.request.params.id);
    }

    getCrewMembers() {
        const id = this.request.params.id;
        return this.planesRepository.getCrewMembers(id);
    }

    saveCrew() {
        const {crew} = this.request.payload;
        return this.planesRepository.saveCrew(crew)
            .then(() => this.planesRepository.getCrewList());
    }

    deleteCrew() {
        const id = this.request.params.id;
        return this.planesRepository.deleteCrew(id)
            .then(() => this.planesRepository.getCrewList());
    }
}