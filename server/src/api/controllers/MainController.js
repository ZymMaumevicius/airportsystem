import BaseController from '../../core/base-controller/baseController';

export default class MainController extends BaseController {
	constructor(...args) {
		super(...args);
		this.db = this.container.get('db');
	}

	index() {
		return 'it works like a charm';
	}
}
