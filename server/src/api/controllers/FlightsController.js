import _ from 'lodash';
import BaseController from '../../core/base-controller/baseController';

export default class FlightsController extends BaseController {
	constructor(...args) {
		super(...args);

		/**
		 * @type FlightRepository
		 */
		this.flightRepository = this.container.get('flightRepository');

		/**
		 * @type ValidationProvider
		 */
		this.validator = this.container.get('validationProvider');
	}

	saveFlight() {
		const { flight } = this.request.payload;
		return this.validator.validate(flight, [
			'start',
			'end',
			'route_num',
			'fuel_consumtion',
			'profit',
			'end_time_zone'])
			.then((result) => this.flightRepository.insert(result))
			.then(() => this.flightRepository.getAllFlights());
	}

	getAll() {
		return this.flightRepository.getAllFlights();
	}

	getAssignable() {
		return this.flightRepository.getAssignable();
	}

	getById() {
		const {id} = this.request.payload;

		return this.flightRepository.getById(id);
	}

	assign() {
		const {id, plane_num} = this.request.payload;

		return this.flightRepository.update({id: id, plane_num: _.parseInt(plane_num)})
			.then(() => this.flightRepository.getAssignable());
	}

	status() {
		return this.flightRepository.getStatusInfo();
	}
}