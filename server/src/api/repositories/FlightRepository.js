
import BaseRepository from '../../core/database/BaseRepository';
import _ from 'lodash';

export default class FlightRepository extends BaseRepository {
	constructor(...args) {
		super(...args);
	}

	insert(flight) {
		return this._db
			.insert(flight)
			.into('public.flight')
			.then(_.identity);
	}

	getAllFlights() {
		return this._db
			.raw(`
				SELECT a."name" as "afname",a.country as "afcountry", aa."name" as "atname",aa.country as "atcountry", f.*
				FROM public.flight f
				JOIN public.route r 
					ON f.route_num = r.id
				JOIN public.airport as "a" on r.airport_from_id = a.id
				JOIN public.airport as "aa" on r.airport_to_id = aa.id
			`).then(({rows}) => rows);
	}

	getById(id) {
		return this._db
			.raw(`
				SELECT a."name" as "afname",a.country as "afcountry", aa."name" as "atname",aa.country as "atcountry", f.*
				FROM public.flight f
				JOIN public.route r 
					ON f.route_num = r.id
				JOIN public.airport as "a" on r.airport_from_id = a.id
				JOIN public.airport as "aa" on r.airport_to_id = aa.id
				WHERE f.id = ${id}
			`).then(({rows}) => rows[0]);
	}

	getAssignable() {
		return this._db
			.raw(`
				SELECT a."name" as "afname",a.country as "afcountry", aa."name" as "atname",aa.country as "atcountry", f.*
				FROM public.flight f
				JOIN public.route r 
					ON f.route_num = r.id
				JOIN public.airport as "a" on r.airport_from_id = a.id
				JOIN public.airport as "aa" on r.airport_to_id = aa.id
				WHERE f.plane_num is null
			`).then(({rows}) => rows);
	}

	update(obj) {
		return this._db
			.raw(`
				UPDATE public.flight SET plane_num = ${obj.plane_num}
				WHERE id = ${obj.id};
			`);
	}

	getStatusInfo() {
		return this._db
			.raw(`
				SELECT 
					a."name" as "afname",
					a.country as "afcountry",
					aa."name" as "atname",
					aa.country as "atcountry",
					p.make as "make",
					p.model as "model",
					p.number as "number",
					fs."name" as "status"
				FROM public.flight f
				JOIN public.route r 
					ON f.route_num = r.id
				JOIN public.airport as "a" on r.airport_from_id = a.id
				JOIN public.airport as "aa" on r.airport_to_id = aa.id
				JOIN public.plane as p on f.plane_num = p.id
				LEFT JOIN public.flight_status fs on f.flight_status = fs.id
				WHERE f.plane_num is not null
			`).then(({rows}) => rows);
	}
}