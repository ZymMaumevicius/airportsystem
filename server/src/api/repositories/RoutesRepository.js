import _ from 'lodash';
import BaseRepository from '../../core/database/BaseRepository';

const AIRPORTS = 'public.airport';
const ROUTES = 'public.route';

export default class RoutesRepository extends BaseRepository {
	constructor(...args) {
		super(...args);
	}

	saveRoute(route) {
		return this._db
			.insert(route)
			.into(ROUTES)
			.debug()
			.then(_.identity);
	}

	getAirports() {
		return this._db
			.select('*')
			.from(AIRPORTS)
			.then(_.identity);
	}

	getFlightInfo() {
		return this._db
			.raw(`
				SELECT a."name" "afname", a.country "afcountry",aa.name "atname", aa.country "atcountry", r.*
				FROM ${ROUTES} as "r"
				JOIN ${AIRPORTS} as "a" on r.airport_from_id = a.id
				JOIN ${AIRPORTS} as "aa" on r.airport_to_id = aa.id
			`)
			.then(({rows}) => rows);
	}
}