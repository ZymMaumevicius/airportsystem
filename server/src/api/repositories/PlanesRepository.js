import _ from 'lodash';
import BaseRepository from '../../core/database/BaseRepository';

const PLANES = 'public.plane';
const BAGGAGE = 'public.plane_baggage';
const TECH_INFO = 'public.technical_checkup';

export default class PlanesRepository extends BaseRepository {
    constructor(...args) {
        super(...args);
    }

    /**
     * Gauti visa bagaza
     */
    getAllBaggage() {
        return this._db
            .raw(`select ${BAGGAGE}.id, ${BAGGAGE}."number", ${BAGGAGE}.weight, width, height, length, ${PLANES}.model, ${PLANES}.make from ${BAGGAGE} left join ${PLANES} on ${PLANES}.id = ${BAGGAGE}.plane_id`)
            .then(({rows}) => rows);
    }

    /**
     * Gauti viena bagaza
     * @param id
     */
    getBaggage(id) {
        return this._db
            .first([
                'id',
                'number',
                'plane_id',
                'weight',
                'width',
                'height',
                'length'
            ])
            .from(BAGGAGE)
            .where('id', id);
    }

    /**
     * Atnaujinti bagazo info
     * @param baggage
     */
    updateBaggage(baggage) {
        return this._db
            .update(baggage)
            .table(BAGGAGE)
            .where('id', baggage.id)
            .then(_.identity);
    }

    /**
     * Issaugoti nauja lektuva
     * @param plane
     */
    savePlane(plane) {
        return this._db
            .insert(plane)
            .into(PLANES)
            .then(_.identity);
    }

    /**
     * Gauti visus lektuvus
     */
    getAllPlanes() {
        return this._db
            .select('*')
            .from(PLANES)
            .then((result) => result);
    }

    /**
     * Gauti vieno lektuvo info pagal id
     * @param id
     */
    getPlaneInfo(id) {
        return this._db
            .first([
                'id',
                'number',
                'make',
                'model'
            ])
            .from(PLANES)
            .where('id', id);
    }

    /**
     * Technines bukles registravimui
      * @param techInfo
     */
    saveTechInfo(techInfo) {
        return this._db
            .insert(techInfo)
            .into(TECH_INFO)
            .then(_.identity);
    }

    /**
     * Lektuvu sarasas su tech apziura
     */
    getAllPlanesWithTech() {
        return this._db
            .raw(`select 
	            ${PLANES}.id,
	            ${PLANES}."number",
	            ${PLANES}.make,
	            ${PLANES}.model,
	            coalesce(to_char(MAX(${TECH_INFO}."date"), 'YYYY-MM-dd'), 'Neatlikta') as last_check,
	            count(${TECH_INFO}.id) as check_count,
	            (select check_is_passed
					from ${TECH_INFO}
					where "date" = (select MAX("date") from ${TECH_INFO} where plane_id = plane.id) and plane_id = plane.id
					limit 1) as success
                from ${PLANES}
                left join ${TECH_INFO} on ${TECH_INFO}.plane_id = ${PLANES}.id
                group by ${PLANES}.id
                order by last_check desc`)
            .then(({rows}) => rows);
    }


    getCrewList() {
        return this._db
            .raw(`select 
                    public.plane_crew.id,
                    public.plane_crew."number",
                    to_char(public.plane_crew.valid_from, 'YYYY-MM-dd') as valid_from,
                    to_char(public.plane_crew.valid_to, 'YYYY-MM-dd') as valid_to,
                    count(public.plane_crew.id) filter (where public.worker.user_type_id = 1) as pilots,
                    count(public.plane_crew.id) filter (where public.worker.user_type_id = 2) as stewards
                from public.crew_members
                right join plane_crew on public.plane_crew.id = public.crew_members.crew_id
                left join worker on public.worker.id = public.crew_members.worker_id
                group by public.plane_crew.id
            `)
            .then(({rows}) => rows);
    }

    getWorkers() {
        return this._db
            .raw(`select
                public.worker.id,
                first_name,
                last_name,
                public.worker_type."name" as "type"
                from public.worker
                inner join public.worker_type on public.worker.user_type_id = public.worker_type.id
                order by last_name
            `)
            .then(({rows}) => rows);
    }

    getCrewInfo(id) {
        return this._db
            .raw(`select
                id,
                "number",
                plane_id,
                to_char(valid_from, 'YYYY-MM-dd') as valid_from,
                to_char(valid_to, 'YYYY-MM-dd') as valid_to,
                is_disabled
                from public.plane_crew
                where id = ${id}
            `)
            .then(({rows}) => rows[0]);
    }

    getCrewMembers(id) {
        return this._db
            .raw(`select
                worker.id,
                worker.first_name,
                worker.last_name,
                worker_type."name" as "type"
                from worker
                inner join worker_type on worker.user_type_id = worker_type.id
                inner join crew_members on crew_members.worker_id = worker.id
                where crew_members.crew_id = ${id}
             `)
            .then(({rows}) => rows);
    }

    saveCrew(crew) {
        if (crew.id > 0) {
            const members = _.map(crew.crewMembers, (member) => (`(${crew.id}, ${member.id})`));
            const insertString = _.join(members, ', ');

            return this._db
                .raw(`update public.plane_crew set
                    "number" = '${crew.number}',
                    valid_from = '${crew.valid_from}',
                    valid_to = '${crew.valid_to}',
                    is_disabled = '${crew.is_disabled}',
                    plane_id = ${crew.plane_id}
                    where id = ${crew.id};
                    
                    delete from public.crew_members where crew_id = ${crew.id};
                    
                    insert into public.crew_members (crew_id, worker_id) values
                    ${insertString};
                    
                `).then(_.identity);
        } else {
            const members = _.map(crew.crewMembers, (member) => (`((select last_value from plane_crew_id_seq), ${member.id})`));
            const insertString = _.join(members, ', ');

            return this._db
                .raw(`insert into public.plane_crew
                    ("number", valid_from, valid_to, is_disabled, plane_id) values
                    ('${crew.number}', '${crew.valid_from}', '${crew.valid_to}', ${crew.is_disabled}, ${crew.plane_id});
                    
                    insert into public.crew_members (crew_id, worker_id) values
                    ${insertString};
                    
                `).then(_.identity);
        }

    }

    deleteCrew(id) {
        return this._db
            .raw(`
                delete from public.crew_members where crew_id = ${id};
                delete from public.plane_crew where id = ${id};
            `)
            .then(_.identity);
    }
}