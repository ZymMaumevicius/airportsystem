import _ from 'lodash';
import BaseRepository from '../../core/database/BaseRepository';

const TABLE_NAME = 'public.user';

export default class AuthRepository extends BaseRepository {
	constructor(...args) {
		super(...args);
	}

	insert(user) {
		return this._db
			.insert(user)
			.into(TABLE_NAME)
			.returning('id');
	}

	getByUsername(username, fields = '*') {
		return this._db
			.first(fields)
			.from(TABLE_NAME)
			.where('email', username)
			.then(_.identity);
	}
}