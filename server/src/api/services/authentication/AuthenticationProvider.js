import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import config from 'config/dev.json';

export default class AuthenticationProvider {
	constructor(authRepository, session) {
		/** @type AuthRepository */
		this.authRepository = authRepository;

		/** @type SessionFactory*/
		this.session = session;
	}

	register(user) {
		return bcrypt.hash(user.password, 10)
			.then((hash) => {
				user.password = hash;

				return this.authRepository.insert(user);
			});
	}

	signIn(username, password) {
		return this.authRepository.getByUsername(username, ['email', 'password', 'rights'])
			.then((user) => bcrypt.compare(password, user.password)
				.then((result) => result ? user : null)
			)
			.then((user) => {
				if (!user) {
					return '';
				}

				delete user.password;

				return jwt.sign({ user }, config.auth.key, { expiresIn: '1 day' });
			});
	}

	validateUser(decodedToken, request) {
		return this.authRepository.getByUsername(decodedToken.user.email, ['*'])
			.then((user) => {
				if (!user) {
					return false;
				}

				return jwt.verify(request.headers['authorization'], config.auth.key);
			});
	}
}