import _ from 'lodash';
import when from 'when';

export default class ValidationProvider {
	constructor() {
	}

	validate(validationObject, rules = []) {
		return when({})
			.then(() => {
				_.forOwn(validationObject, (value, key) => {
					if (!_.includes(rules, key)) {
						throw new Error('JSON is not VALID');
					}
				});

				return validationObject;
			});
	}
}