const gulp = require('gulp');
const babel = require('gulp-babel');
const del = require('del');
const runSequence = require('run-sequence');
const sass = require('gulp-sass');
const path = require('path');
const gutil = require('gulp-util');

const publicPath = path.join(__dirname, '..', 'server', 'dist');
const webpackDir = path.join(__dirname, '..', 'node_modules', 'webpack', 'bin', 'webpack.js');

gulp.task('clean', (cb) => {
	del(['./dist/**'])
		.then(() => cb());
});

gulp.task('sass', function () {
    return gulp.src('./src/sass/**/*.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest(publicPath + '/public/css'));
});

gulp.task('sass:watch', function () {
	return gulp.watch('./src/sass/**/*.scss', ['sass']);
});

gulp.task('babelify', function () {
	return gulp.src(['./src/index.js'])
		.pipe(
			babel({
					presets: ['es2016-node5', 'react'],
					plugins: [
						['transform-class-properties'],
						['module-resolver', {
							alias: { src: './src' }
						}],
						['transform-decorators-legacy']
					]
				}
			)
		)
		.pipe(gulp.dest(publicPath + '/app.js'));
});

gulp.task('babelify:watch', function() {
	return gulp.watch('./src/**/*.js', ['babelify']);

});

gulp.task('build',(done) => {
	runSequence(
		'clean',
		'sass',
		'pack',
		done
	);
});

gulp.task('watch', ['build']);

gulp.task('pack', (done) => {
	const spawn = require('child_process').spawn;
	const readline = require('readline');
	const webpackProcess = spawn(
		'node',
		[webpackDir],
		{
			cwd: __dirname,
			env: process.env
		}
	);

	readline.createInterface({input: webpackProcess.stdout})
		.on('line', (line) => gutil.log(line));

	readline.createInterface({input: webpackProcess.stderr})
		.on('line', (line) => gutil.log(line));

	webpackProcess.on('exit', (code, signal) => {
		done(code || signal ? new Error('Webpack encountered an error.') : undefined);
	});
});

