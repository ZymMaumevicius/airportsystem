const path = require('path');

module.exports = {
	watch: true,
	entry: './src/index.js',
	output: { path: path.join(__dirname, '..', 'server','dist','public'), filename: 'app.js' },
	module: {
		loaders: [
			{
				test: /.js?$/,
				loader: 'babel-loader',
				exclude: /node_modules/,
				query: {
					presets: ['es2015', 'react']
				}
			}
		]
	},
	devtool: '#inline-source-map'
};