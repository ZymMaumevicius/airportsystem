import React, {Component} from 'react';
import {connect} from 'react-redux';
import _ from 'lodash';
import PlaneActions from '../../../actions/PlaneActions';

class Create extends Component {
    constructor() {
        super();

        this.state = {
            number: '',
            date: '',
            check_is_passed: false,
            comments: '',
            chassis: false,
            flaps: false,
            steering_mechanism: false,
            landing_gear: false,
            engines: false,
            fuel_system: false,
            communications: false,
            lights: false,
            brakes: false,
            plane_id: ''
        };
    }

    placeHolders() {
        return {
            number: 'Patikros numeris',
            date: 'Data',
            check_is_passed: 'Patikra praeita',
            comments: 'Pastabos',
            chassis: 'Korpusas',
            flaps: 'Užsparniai',
            steering_mechanism: 'Vairo mechanizmas',
            landing_gear: 'Važiuoklė',
            engines: 'Varikliai',
            fuel_system: 'Kuro sistema',
            communications: 'Ryšio įranga',
            lights: 'Šviesos',
            brakes: 'Stabdžiai',
            plane_id: ''
        };
    }

    componentWillMount() {
        this.props.dispatch(PlaneActions.getPlaneInfo(this.props.params.id));
    }

    render() {
        const planeInfo = this.props.planes.planeInfo;
        return (
            <div className="container">
                <div className="row">
                    <div className="col-sm-6">
                        <h4>Lėktuvo {planeInfo.make} {planeInfo.model} (Nr. {planeInfo.number}) techninė apžiūra</h4>

                        <div className="form-group">
                            <label>{this.placeHolders().number}</label>
                            <input type="text" id="number" className="form-control" onChange={(e) => this.handleChange(e)}/>
                        </div>

                        <div className="form-group">
                            <label>{this.placeHolders().date}</label>
                            <input type="date" id="date" className="form-control" onChange={(e) => this.handleChange(e)}/>
                        </div>
                        <div className="form-group">
                            <label>{this.placeHolders().comments}</label>
                            <input type="text" id="comments" className="form-control" onChange={(e) => this.handleChange(e)}/>
                        </div>

                        <table>
                            <tbody>
                            <tr>
                                <td><input type="checkbox" id="chassis" className="checkbox-tabled" onChange={(e) => this.handleCheckBox(e)}/></td>
                                <td>{this.placeHolders().chassis}</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" id="flaps" className="checkbox-tabled" onChange={(e) => this.handleCheckBox(e)}/></td>
                                <td>{this.placeHolders().flaps}</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" id="steering_mechanism" className="checkbox-tabled" onChange={(e) => this.handleCheckBox(e)}/></td>
                                <td>{this.placeHolders().steering_mechanism}</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" id="landing_gear" className="checkbox-tabled" onChange={(e) => this.handleCheckBox(e)}/></td>
                                <td>{this.placeHolders().landing_gear}</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" id="engines" className="checkbox-tabled" onChange={(e) => this.handleCheckBox(e)}/></td>
                                <td>{this.placeHolders().engines}</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" id="fuel_system" className="checkbox-tabled" onChange={(e) => this.handleCheckBox(e)}/></td>
                                <td>{this.placeHolders().fuel_system}</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" id="communications" className="checkbox-tabled" onChange={(e) => this.handleCheckBox(e)}/></td>
                                <td>{this.placeHolders().communications}</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" id="lights" className="checkbox-tabled" onChange={(e) => this.handleCheckBox(e)}/></td>
                                <td>{this.placeHolders().lights}</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" id="brakes" className="checkbox-tabled" onChange={(e) => this.handleCheckBox(e)}/></td>
                                <td>{this.placeHolders().brakes}</td>
                            </tr>
                            <tr>
                                <td style={{height: '64px'}}><input type="checkbox" id="check_is_passed" className="checkbox-tabled" onChange={(e) => this.handleCheckBox(e)}/></td>
                                <td>{this.placeHolders().check_is_passed}</td>
                            </tr>
                            </tbody>
                        </table>

                        {this.state.error && (<div className="alert alert-danger" role="alert">{this.state.error}</div>)}
                        <button className="btn btn-primary" type="button" onClick={() => this.onSubmit()}>Saugoti</button>
                    </div>
                </div>
            </div>
        );
    }

    handleChange(e) {
        const currentState = this.state;
        currentState[e.target.id] = e.target.value;
        this.setState(currentState);
    }

    handleCheckBox(e) {
        const currentState = this.state;
        currentState[e.target.id] = e.target.checked;
        this.setState(currentState);
    }

    onSubmit() {
        if (this.state.plane_id <= 0) {
            const state = this.state;
            state.plane_id = this.props.planes.planeInfo.id;
            this.setState(state);
        }

        let empty;
        const currentState = this.state;
        _.forOwn(currentState, (value, key) => {
            if (value.length === 0) {
                empty = key;
            }
        });

        if (empty) {
            this.setState({
                error: 'Neįvestas laukas ' + this.placeHolders()[empty]
            });
            return;
        }

        delete currentState.error;

        this.setState(currentState);
        this.props.dispatch(PlaneActions.saveTechData(currentState));
    }
}

function mapStoreToProps(store) {
    return {
        planes: store.planes
    };
}

export default connect(mapStoreToProps)(Create);