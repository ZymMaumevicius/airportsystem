import React, {Component} from 'react';
import {connect} from 'react-redux';
import ReactTable from 'react-table';
import {Link} from 'react-router';
import PlaneActions from '../../../actions/PlaneActions';

class Display extends Component {
    constructor() {
        super();
    }

    componentWillMount() {
        this.props.dispatch(PlaneActions.getTechPlanes());
    }

    render() {
        return (
            <div>
                <ReactTable
                    data={this.props.planes.techPlanes}
                    columns={[
                        {
                            Header: '',
                            id: 'plane_id',
                            accessor: (d) => d.id,
                            Cell: (row) => (
                                <Link to={`/planes/tech/create/${row.value}`}>Pildyti naują</Link>
                            )
                        },
                        {
                            Header: 'Lėktuvas',
                            id: 'plane',
                            accessor: (d) => `${d.make} ${d.model}`
                        },
                        {
                            Header: 'Numeris',
                            id: 'number',
                            accessor: (d) => d.number
                        },
                        {
                            Header: 'Paskutinis patikrinimas',
                            id: 'last_check',
                            accessor: (d) => d.last_check
                        },
                        {
                            Header: 'Kiek kartų tikrinta',
                            id: 'ckeck_count',
                            accessor: (d) => d.check_count
                        },
                        {
                            Header: 'Apžiūra praeita',
                            id: 'success',
                            accessor: (d) => `${d.success ? 'Praeita' : 'Nepraeita'}`
                        }
                    ]}
                />
            </div>
        );
    }
}

function mapStoreToProps(store) {
    return {
        planes: store.planes
    };
}

export default connect(mapStoreToProps)(Display);