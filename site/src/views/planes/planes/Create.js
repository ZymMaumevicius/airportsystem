import _ from 'lodash';
import React, { Component } from 'react';
import PlaneActions from '../../../actions/PlaneActions';
import {connect} from 'react-redux';

class Create extends Component {
    constructor() {
        super();

        this.state = {
            number: '',
            model: '',
            make: '',
            weight: '',
            lifting_power: '',
            baggage_volume: '',
            business_seats: '',
            economy_seats: '',
            pilots: '',
            stewards: '',
            toilets: ''
        };
    }

    placeHolders() {
        return {
            number: 'Lėktuvo nr.',
            model: 'Modelis',
            make: 'Markė',
            weight: 'Svoris (kg)',
            lifting_power: 'Keliammoji galia (kg)',
            baggage_volume: 'Bagažo tūris (m3)',
            business_seats: 'Verslo kl. vietos',
            economy_seats: 'Ekonominės kl. vietos',
            pilots: 'Pilotai',
            stewards: 'Stiuardai',
            toilets: 'Tualetai'
        };
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-6 col-md-offset-6">
                        <div className="form-group">
                            <label>{this.placeHolders().number}</label>
                            <input type="text" id="number" className="form-control" placeholder={this.placeHolders().number} onChange={(e) => this.handleChange(e)}/>
                        </div>

                        <div className="form-group">
                            <label>{this.placeHolders().make}</label>
                            <input type="text" id="make" className="form-control" placeholder={this.placeHolders().make} onChange={(e) => this.handleChange(e)}/>
                        </div>

                        <div className="form-group">
                            <label>{this.placeHolders().model}</label>
                            <input type="text" id="model" className="form-control" placeholder={this.placeHolders().model} onChange={(e) => this.handleChange(e)}/>
                        </div>
                        <div className="form-group">
                            <label>{this.placeHolders().weight}</label>
                            <input type="number" id="weight" className="form-control" placeholder={this.placeHolders().weight} onChange={(e) => this.handleChange(e)}/>
                        </div>

                        <div className="form-group">
                            <label>{this.placeHolders().lifting_power}</label>
                            <input type="number" id="lifting_power" className="form-control" placeholder={this.placeHolders().lifting_power} onChange={(e) => this.handleChange(e)}/>
                        </div>

                        <div className="form-group">
                            <label>{this.placeHolders().baggage_volume}</label>
                            <input type="number" id="baggage_volume" className="form-control" placeholder={this.placeHolders().baggage_volume} onChange={(e) => this.handleChange(e)}/>
                        </div>

                        <div className="form-group">
                            <label>{this.placeHolders().business_seats}</label>
                            <input type="number" id="business_seats" className="form-control" placeholder={this.placeHolders().business_seats} onChange={(e) => this.handleChange(e)}/>
                        </div>

                        <div className="form-group">
                            <label>{this.placeHolders().economy_seats}</label>
                            <input type="number" id="economy_seats" className="form-control" placeholder={this.placeHolders().economy_seats} onChange={(e) => this.handleChange(e)}/>
                        </div>

                        <div className="form-group">
                            <label>{this.placeHolders().pilots}</label>
                            <input type="number" id="pilots" className="form-control" placeholder={this.placeHolders().pilots} onChange={(e) => this.handleChange(e)}/>
                        </div>
                        <div className="form-group">
                            <label>{this.placeHolders().stewards}</label>
                            <input type="number" id="stewards" className="form-control" placeholder={this.placeHolders().stewards} onChange={(e) => this.handleChange(e)}/>
                        </div>
                        <div className="form-group">
                            <label>{this.placeHolders().toilets}</label>
                            <input type="number" id="toilets" className="form-control" placeholder={this.placeHolders().toilets} onChange={(e) => this.handleChange(e)}/>
                        </div>

                        {this.state.error && (
                            <div className="alert alert-danger">{this.state.error}</div>
                        )}
                        <button className="btn btn-primary" onClick={() => this.onSubmit()}>Išsaugoti</button>
                    </div>
                </div>
            </div>
        );
    }

    onSubmit() {
        let empty;

        const currentState = this.state;
        _.forOwn(currentState, (value, key) => {
            if (value.length === 0) {
                empty = key;
            }
        });

        if (empty) {
            this.setState({
                error: 'Neįvestas laukas ' + this.placeHolders()[empty]
            });

            return;
        }

        delete currentState.error;
        this.setState(currentState);

        this.props.dispatch(PlaneActions.createPlane(currentState));
    }

    handleChange(e) {
        const currentState = this.state;
        currentState[e.target.id] = e.target.value;
        this.setState(currentState);
    }
}

export default connect()(Create);