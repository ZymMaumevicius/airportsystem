import React, {Component} from 'react';
import {connect} from 'react-redux';
import ReactTable from 'react-table';
import {Link} from 'react-router';
import PlaneActions from '../../../actions/PlaneActions';

// const data = [
//     {
//         number: '3',
//         model: '6',
//         make: '4',
//         weight: '5',
//         lifting_power: '4',
//         baggage_volume: '7',
//         business_seats: '8',
//         economy_seats: '6',
//         pilots: '4',
//         stewards: '3',
//         toilets: '1'
//     }
// ];

class Display extends Component {
    constructor() {
        super();
    }

    componentWillMount() {
        this.props.dispatch(PlaneActions.getAllPlanes());
    }

    render() {
        return (
            <div>
                <Link to="/planes/newplane">Užregistruoti lėktuvą</Link>
                <ReactTable
                    data={this.props.planes.planes}
                    columns={[
                        {
                            Header: 'Numeris',
                            id: 'number',
                            accessor: (d) => d.number
                        },
                        {
                            Header: 'Markė',
                            id: 'make',
                            accessor: (d)=> d.make
                        },
                        {
                            Header: 'Modelis',
                            id: 'model',
                            accessor: (d) => d.model
                        },
                        {
                            Header: 'Svoris',
                            id: 'weight',
                            accessor: (d) => d.weight
                        },
                        {
                            Header: 'Keleivių sk.',
                            id: 'passengers',
                            accessor: (d) => d.business_seats + d.economy_seats
                        },
                        {
                            Header: 'Pilotai',
                            id: 'pilots',
                            accessor: (d) => d.pilots
                        },
                        {
                            Header: 'Stiuardai',
                            id: 'pilots',
                            accessor: (d) => d.stewards
                        }
                    ]}
                    defaultPageSize={10}
                    className="-stripped -highlight"
                />
            </div>
        );
    }
}

function mapStoreToProps(store) {
    return {
        planes: store.planes
    };
}

export default connect(mapStoreToProps)(Display);