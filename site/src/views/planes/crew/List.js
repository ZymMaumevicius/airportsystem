import React, {Component} from 'react';
import {connect} from 'react-redux';
import ReactTable from 'react-table';
import {Link} from 'react-router';
import PlaneActions from '../../../actions/PlaneActions';

class List extends Component {
    constructor() {
        super();
    }

    componentWillMount() {
        this.props.dispatch(PlaneActions.getCrewList());
    }

    render() {
        return (
            <div>
                <Link to="/planes/crew/edit/0">Užregistruoti naują įgulą</Link>
                <ReactTable
                    data={this.props.planes.crewList}
                    columns={[
                        {
                            Header: '',
                            id: 'edit',
                            accessor: (d) => d.id,
                            Cell: (row) => (
                                <Link to={`/planes/crew/edit/${row.value}`}>Redaguoti</Link>
                            )
                        },
                        {
                            Header: '',
                            id: 'delete',
                            accessor: (d) => d.id,
                            Cell: (row) => (
                                <a href="#" id onClick={() => this.deleteCrew(row.value) }>Ištrinti</a>
                            )
                        },
                        {
                            Header: 'Įgulos numeris',
                            id: 'number',
                            accessor: (d) => d.number
                        },
                        {
                            Header: 'Galioja nuo',
                            id: 'valid_from',
                            accessor: (d) => d.valid_from
                        },
                        {
                            Header: 'Galioja iki',
                            id: 'valid_to',
                            accessor: (d) => d.valid_to
                        },
                        {
                            Header: 'Pilotai',
                            id: 'pilots',
                            accessor: (d) => d.pilots
                        },
                        {
                            Header: 'Stiuardai',
                            id: 'stewards',
                            accessor: (d) => d.stewards
                        }
                    ]}
                />
            </div>
        );
    }

    deleteCrew(id) {
        this.props.dispatch(PlaneActions.deleteCrew(id));
    }
}

function mapStoreToProps(store) {
    return {
        planes: store.planes
    };
}

export default connect(mapStoreToProps)(List);
