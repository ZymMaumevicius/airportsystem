import React, {Component} from 'react';
import {connect} from 'react-redux';
import _ from 'lodash';
import PlaneActions from '../../../actions/PlaneActions';

class Edit extends Component {
    constructor() {
        super();
        this.added = false;
        this.workersAdded = false;
        this.membersAdded = false;

        this.state = {
            id: '',
            number: '',
            valid_from: '',
            valid_to: '',
            is_disabled: '',
            plane_id: '',
            workersAvailable: [],
            crewMembers: []
        };
    }

    placeHolders() {
        return {
            id: '',
            number: 'Numeris',
            valid_from: 'Galioja nuo',
            valid_to: 'Galioja iki',
            is_disabled: 'Neaktyvi įgula',
            plane_id: 'Lėktuvas',
            workersAvailable: 'Galimi darbuotojai',
            crewMembers: 'Priskirti darbuotojai'
        };
    }

    componentWillMount() {
        this.props.dispatch(PlaneActions.getAllPlanes());
        this.props.dispatch(PlaneActions.getCrewMembers(this.props.params.id));
        this.props.dispatch(PlaneActions.getWorkers());

        if (this.props.params.id > 0) {
            this.props.dispatch(PlaneActions.getCrew(this.props.params.id));
        } else {
            this.props.planes.crewInfo = {};
        }
    }

    componentWillReceiveProps(props) {
        const state = this.state;
        const info = props.planes.crewInfo;
        state.id = info.id || 0;
        state.number = info.number || '';
        state.valid_from = info.valid_from || '';
        state.valid_to = info.valid_to || '';
        state.is_disabled = info.is_disabled || false;
        state.plane_id = info.plane_id || '';

        state.workersAvailable = props.planes.workersAvailable;
        state.crewMembers = props.planes.crewMembers;

        // if (!this.added && props.planes.crewInfo.id) {
        //     // this.setState(props.planes.crewInfo);
        //     const state = this.state;
        //     const info = props.planes.crewInfo;
        //     state.id = info.id;
        //     state.number = info.number;
        //     state.valid_from = info.valid_from;
        //     state.valid_to = info.valid_to;
        //     state.is_disabled = info.is_disabled;
        //     state.plane_id = info.plane_id;
        //     console.log('plane info added');
        //     console.log(state);
        //
        //     this.setState(state);
        //     this.added = true;
        // }
        //
        // if (!this.workersAdded && props.planes.workersAvailable.length > 0) {
        //     console.log('adding worker list');
        //
        //     const state = this.state;
        //     state.workersAvailable = props.planes.workersAvailable;
        //     this.setState(state);
        //     this.workersAdded = true;
        // }
        //
        // if (!this.membersAdded && props.planes.crewMembers.length > 0) {
        //     console.log('adding crew list');
        //
        //     const state = this.state;
        //     state.crewMembers = props.planes.crewMembers;
        //     this.setState(state);
        //     this.membersAdded = true;
        // }
    }

    render() {
        const state = this.state;

        const planeOptions = _.map(this.props.planes.planes, (value) => {
            return (<option key={value.id} value={value.id}>{`${value.make} - ${value.model}`}</option>);
        });

        const workersAvailable = _.differenceBy(state.workersAvailable, state.crewMembers, 'id');

        const workers = _.map(workersAvailable, (value) => {
            return (<li key={value.id} className="list-group-item" id={value.id} onClick={(e) => this.addWorker(e)}>{`${value.first_name} ${value.last_name}, ${value.type}`} &rarr;</li>);
        });

        const crew = _.map(this.props.planes.crewMembers, (value) => {
            return (<li key={value.id} className="list-group-item" id={value.id} onClick={(e) => this.removeWorker(e)}>&larr; {`${value.first_name} ${value.last_name}, ${value.type}`}</li>);
        });

        return (
            <div className="container">
                <div className="row">
                    <div className="col-sm-10 col-sm-offset-2">
                        <div className="form-group">
                            <label>{this.placeHolders().number}</label>
                            <input type="text" id="number" className="form-control" value={state.number} onChange={(e) => this.handleChange(e)}/>
                        </div>

                        <div className="form-group">
                            <label>{this.placeHolders().plane_id}</label>
                            <select id="plane_id" className="form-control" value={state.plane_id} onChange={(e) => this.handleChange(e)}>{planeOptions}</select>
                        </div>

                        <div className="form-group">
                            <label>{this.placeHolders().valid_from}</label>
                            <input type="date" id="valid_from" className="form-control" value={state.valid_from} placeholder={this.placeHolders().valid_from} onChange={(e) => this.handleChange(e)}/>
                        </div>

                        <div className="form-group">
                            <label>{this.placeHolders().valid_to}</label>
                            <input type="date" id="valid_to" className="form-control" value={state.valid_to} placeholder={this.placeHolders().valid_to} onChange={(e) => this.handleChange(e)}/>
                        </div>

                        <div className="form-group">
                            <table>
                                <tbody>
                                <tr>
                                    <td><input type="checkbox" id="is_disabled" className="checkbox-tabled" onChange={(e) => this.handleCheckBox(e)}/></td>
                                    <td>{this.placeHolders().is_disabled}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                        <hr/>

                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h4>Įgulos narių priskyrimas</h4>
                            </div>

                            <div className="panel-body">
                                <div className="row">

                                    <div className="col-sm-6">
                                        <ul className="listGroup">
                                            <li className="list-group-item bg-warning">Galimi darbuotojai:</li>
                                            {workers.length > 0 ? '' : (<li className="list-group-item">Tuščia</li>)}
                                            {workers}
                                        </ul>
                                    </div>

                                    <div className="col-sm-6">
                                        <ul className="listGroup">
                                            <li className="list-group-item bg-warning">Priskirti darbuotojai:</li>
                                            {crew.length > 0 ? '' : (<li className="list-group-item">Nieko nepriskirta.</li>)}
                                            {crew}
                                        </ul>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div className="form-group">
                            {this.state.error && (<div className="alert alert-danger" role="alert">{this.state.error}</div>)}
                        </div>

                        <div className="form-group">
                            <button className="btn btn-primary" onClick={() => this.onSubmit()}>Saugoti</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    onSubmit() {
        let empty;
        const currentState = this.state;

        _.forOwn(currentState, (value, key) => {
            if (value.length === 0) {
                empty = key;
            }
        });

        if (empty) {
            this.setState({
                error: 'Neįvestas laukas ' + this.placeHolders()[empty]
            });
            return;
        }

        delete currentState.error;

        this.props.dispatch(PlaneActions.saveCrewInfo(currentState));
    }

    handleChange(e) {
        const currentState = this.state;
        currentState[e.target.id] = e.target.value;
        this.setState(currentState);
    }

    handleCheckBox(e) {
        const currentState = this.state;
        currentState[e.target.id] = e.target.checked;
        this.setState(currentState);
    }

    addWorker(e) {
        const id = e.target.id;
        const state = this.state;
        const worker = _.find(state.workersAvailable, (w) => w.id === _.parseInt(id));

        state.crewMembers.push(worker);
        this.setState(state);
    }

    removeWorker(e) {
        const id = e.target.id;
        const state = this.state;

        _.remove(state.crewMembers, (m) => m.id === _.parseInt(id));
        this.setState(state);
    }
}

function mapStoreToProps(store) {
    return {
        planes: store.planes
    };
}

export default connect(mapStoreToProps)(Edit);