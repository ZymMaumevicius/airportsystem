import React, { Component } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';

import AuthorizationActions from '../../actions/AuthorizationActions';
import AuthEnum from '../../../../shared/AuthEnum';

class PlaneWrapper extends Component {
    constructor() {
        super();
    }

    componentWillMount() {
        this.props.dispatch(AuthorizationActions.authorize(AuthEnum.Planes));
    }

    render() {
        return (
            <div>
                <nav>
                    <ul className="nav nav-tabs">
                        <li className="nav-item" ><Link className="nav-link" activeClassName="active" to="/planes/baggage/list">Bagažo priskyrimas lėktuvui</Link></li>
                        <li className="nav-item" ><Link className="nav-link" activeClassName="active" to="/planes/planeslist">Lėktuvų informacijos pildymas</Link></li>
                        <li className="nav-item" ><Link className="nav-link" activeClassName="active" to="/planes/tech/list">Lėktuvų techninės būklės registravimas</Link></li>
                        <li className="nav-item" ><Link className="nav-link" activeClassName="active" to="/planes/crew/list">Įgulos organizavimas</Link></li>
                        {/*<li className="nav-item" ><a className="nav-link" href="http://localhost:3000/planes">Refresh</a></li>*/}
                    </ul>
                </nav>
                <div style={{marginTop: '16px'}}>
                    {this.props.children}
                </div>
            </div>
        );
    }
}

function mapStoreToProps(store) {
    return {
        user: store.user
    };
}

export default connect(mapStoreToProps)(PlaneWrapper);