import React, {Component} from 'react';
import {connect} from 'react-redux';
import ReactTable from 'react-table';
import {Link} from 'react-router';
import PlaneActions from '../../../actions/PlaneActions';

class Display extends Component {
    constructor() {
        super();
    }

    componentWillMount() {
        this.props.dispatch(PlaneActions.getAllBaggage());
    }

    render() {
        return (
            <div>
                <ReactTable
                    data={this.props.planes.baggage}
                    columns={[
                        {
                            Header: '',
                            id: 'selector',
                            accessor: (d) => d.id,
                            Cell: (row) => (
                                <Link to={`/planes/baggage/edit/${row.value}`}>Pasirinkti</Link>
                            )
                        },
                        {
                            Header: 'Numeris',
                            id: 'number',
                            accessor: (d) => d.number
                        },
                        {
                            Header: 'Lėktuvas',
                            id: 'plane',
                            accessor: (d) => `${d.make} ${d.model}`
                        },
                        {
                            Header: 'Svoris (kg)',
                            id: 'weight',
                            accessor: (d) => d.weight
                        },
                        {
                            Header: 'Plotis (cm)',
                            id: 'number',
                            accessor: (d) => d.width
                        },
                        {
                            Header: 'Aukštis (cm)',
                            id: 'height',
                            accessor: (d) => d.height
                        },
                        {
                            Header: 'Ilgis (cm)',
                            id: 'length',
                            accessor: (d) => d.length
                        }
                    ]}
                />
            </div>

        );
    }
}

function mapStoreToProps(store) {
    return {
        planes: store.planes
    };
}

export default connect(mapStoreToProps)(Display);