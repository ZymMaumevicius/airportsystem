import _ from 'lodash';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import PlaneActions from '../../../actions/PlaneActions';

class Edit extends Component {
    constructor() {
        super();
        this.added = false;

        this.state = {
            id: '',
            number: '',
            weight: '',
            width: '',
            height: '',
            length: '',
            plane_id: ''
        };
    }

    placeHolders() {
        return {
            number: 'Bagažo nr.',
            weight: 'Svoris (kg)',
            width: 'Plotis (cm)',
            height: 'Aukštis (cm)',
            length: 'Ilgis (cm)',
            plane_id: 'Lėktuvas'
        };
    }

    componentWillMount() {
        const id = this.props.params.id;

        this.props.dispatch(PlaneActions.getAllPlanes());
        this.props.dispatch(PlaneActions.getBaggage(id));
    }

    componentWillReceiveProps(props) {
        if (!this.added && props.planes.baggageItem.id) {
            this.setState(props.planes.baggageItem);
            this.added = true;
        }
    }

    render() {
        const planeOptions = _.map(this.props.planes.planes, (value) => {
            return (<option key={value.id} value={value.id}>{`${value.make} - ${value.model}`}</option>);
        });

        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-6 col-md-offset-6">
                        <div className="form-group">
                            <label>{this.placeHolders().number}</label>
                            <input type="text" id="number" className="form-control" value={this.state.number} placeholder={this.placeHolders().number} onChange={(e) => this.handleChange(e)}/>
                        </div>

                        <div className="form-group">
                            <label>{this.placeHolders().plane_id}</label>
                            <select id="plane_id" className="form-control" value={this.state.plane_id} onChange={(e) => this.handleChange(e)}>{planeOptions}</select>
                        </div>

                        <div className="form-group">
                            <label>{this.placeHolders().weight}</label>
                            <input type="number" id="weight" className="form-control" value={this.state.weight} placeholder={this.placeHolders().weight} onChange={(e) => this.handleChange(e)}/>
                        </div>
                        <div className="form-group">
                            <label>{this.placeHolders().width}</label>
                            <input type="number" id="width" className="form-control" value={this.state.width} placeholder={this.placeHolders().width} onChange={(e) => this.handleChange(e)}/>
                        </div>
                        <div className="form-group">
                            <label>{this.placeHolders().height}</label>
                            <input type="number" id="height" className="form-control" value={this.state.height} placeholder={this.placeHolders().height} onChange={(e) => this.handleChange(e)}/>
                        </div>
                        <div className="form-group">
                            <label>{this.placeHolders().length}</label>
                            <input type="number" id="length" className="form-control" value={this.state.length} placeholder={this.placeHolders().length} onChange={(e) => this.handleChange(e)}/>
                        </div>

                        {this.state.error && (<div className="alert alert-danger" role="alert">{this.state.error}</div>)}
                        <button className="btn btn-primary" onClick={() => this.onSubmit()}>Saugoti</button>
                    </div>
                </div>
            </div>
        );
    }

    onSubmit() {
        let empty;
        const currentState = this.state;
        _.forOwn(currentState, (value, key) => {
            if (value.length === 0) {
                empty = key;
            }
        });

        if (empty) {
            this.setState({
                error: 'Neįvestas laukas ' + this.placeHolders()[empty]
            });
            return;
        }

        delete currentState.error;

        this.props.planes.baggageItem = this.state;
        this.props.dispatch(PlaneActions.updateBaggage(currentState));
    }

    handleChange(e) {
        const currentState = this.state;
        currentState[e.target.id] = e.target.value;
        this.setState(currentState);
    }
}

function mapStateToProps(store) {
    return {
        planes: store.planes
    };
}

export default connect(mapStateToProps)(Edit);