import _ from 'lodash';

import React, { Component } from 'react';
import { connect } from 'react-redux';

import RouteActions from '../../../actions/RouteActions';

class Create extends Component {
	constructor() {
		super();

		this.state = {
			airport_from_id: '',
			airport_to_id: '',
			valid_from: '',
			valid_to: '',
			planned_traffic: '',
			fuel_consumption_coef: '',
			distance: '',
			airtime_duration: ''
		};
	}

	componentWillMount() {
		this.props.dispatch(RouteActions.getAirports());
	}

	render() {
		const optionsFrom = _.map(this.props.routeInfo.airports, (value) => {
			return (<option key={value.id} value={value.id}>{`${value.city} ${value.name}`}</option>);
		});

		const optionTo = _(this.props.routeInfo.airports)
			.filter((port) => port.id != this.state.airport_from_id)
			.map((value) => (<option key={value.id} value={value.id}>{`${value.city} ${value.name}`}</option>))
			.value();

		return (
			<div className="container">
				<div className="row">
					<div className="col-md-6 col-md-offset-6 text-center">
						<select id="airport_from_id" className="form-control" placeholder="Airport from"
								onChange={(e) => this.handleChange(e)}>
							{optionsFrom}
						</select>
						<select id="airport_to_id" className="form-control" placeholder="Airport to"
								onChange={(e) => this.handleChange(e)}>
							{optionTo}
						</select>
						<input type="date" id="valid_from" className="form-control" placeholder="Valid from"
							   onChange={(e) => this.handleChange(e)}/>
						<input type="date" id="valid_to" className="form-control" placeholder="Valid to"
							   onChange={(e) => this.handleChange(e)}/>
						<input type="number" id="planned_traffic" className="form-control" placeholder="Planned traffic"
							   onChange={(e) => this.handleChange(e)}/>
						<input type="number" id="fuel_consumption_coef" className="form-control"
							   placeholder="Fuel consumption coef."
							   onChange={(e) => this.handleChange(e)}/>
						<input type="number" id="distance" className="form-control" placeholder="Distance"
							   onChange={(e) => this.handleChange(e)}/>
						<input type="number" id="airtime_duration" className="form-control" placeholder="Airtime"
							   onChange={(e) => this.handleChange(e)}/>
						{this.state.error && (
							<div className="alert alert-danger" role="alert">{this.state.error}</div>)}
						{this.props.error.error && (
							<div className="alert alert-danger" role="alert">{this.props.error.error}</div>)}
						<button className="btn btn-primary" onClick={() => this.onSubmit()}> GO</button>
					</div>
				</div>
			</div>
		);
	}

	onSubmit() {
		let empty;
		const currentState = this.state;
		_.forOwn(currentState, (value, key) => {
			if (value.length === 0) {
				empty = key;
			}
		});

		if (empty) {
			this.setState({
				airport_from_id: '',
				airport_to_id: '',
				valid_from: '',
				valid_to: '',
				planned_traffic: '',
				fuel_consumption_coef: '',
				distance: '',
				airtime_duration: '',
				error: 'Enter your ' + empty
			});

			return;
		}

		delete currentState.error;

		this.props.dispatch(RouteActions.createRoute(currentState));
	}

	handleChange(e) {
		const currentState = this.state;
		currentState[e.target.id] = e.target.value;
		this.setState(currentState);
	}
}

function mapStateToProps(store) {
	return {
		error: store.error,
		routeInfo: store.routes
	};
}

export default connect(mapStateToProps)(Create);