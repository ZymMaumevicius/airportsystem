import React, { Component } from 'react';
import ReactTable from 'react-table';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import moment from 'moment';

import RouteActions from '../../../actions/RouteActions';

class Display extends Component {
	constructor() {
		super();
	}

	componentWillMount() {
		this.props.dispatch(RouteActions.getRoutesInfo());
	}

	render() {
		return (
			<div>
				<Link className="btn btn-primary" to="/flights/routes/create">Sukurti maršrutą</Link>
				<ReactTable
					data={this.props.routes.routes}
					columns={[
						{
							Header: 'Iš oro uosto',
							id: 'airport_from_id',
							accessor: d => d.afcountry + ' ' + d.afname
						},
						{
							Header: 'Į oro uostą',
							id: 'airport_to_id',
							accessor: d => d.atcountry + ' ' + d.atname
						},
						{
							Header: 'Galioja nuo',
							id: 'valid_from',
							accessor: d => moment(d.valid_from).format('L')
						},
						{
							Header: 'Galioja iki',
							id: 'valid_to',
							accessor: d => moment(d.valid_to).format('L')
						},
						{
							Header: 'Planuojamas judėjimas',
							accessor: 'planned_traffic'
						},
						{
							Header: 'Degalu sąnaudų koeficientas',
							accessor: 'fuel_consumption_coef'
						},
						{
							Header: 'Atstumas',
							accessor: 'distance'
						},
						{
							Header: 'Grynas oro laikas',
							accessor: 'airtime_duration'
						}
					]}
					defaultPageSize={10}
					className="-striped -highlight"
				/>
			</div>
		);
	}
}

function mapStoreToProps(store) {
	return {
		routes: store.routes
	};
}

export default connect(mapStoreToProps)(Display);