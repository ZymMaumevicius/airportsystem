import React, { Component } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';

import AuthEnum from '../../../../shared/AuthEnum';
import AuthorizationActions from '../../actions/AuthorizationActions';

class FlightWrapper extends Component {
	constructor() {
		super();
	}

	componentWillMount() {
		this.props.dispatch(AuthorizationActions.authorize(AuthEnum.Flights));
	}

	render() {
		return (
			<div>
				<nav>
				<ul className="nav nav-tabs">
					<li className="nav-item" ><Link className="nav-link" activeClassName="active" to="/flights/flights">Skrydžių kūrimas</Link></li>
					<li className="nav-item" ><Link className="nav-link" activeClassName="active" to="/flights/routes">Maršrutų kūrimas</Link></li>
					<li className="nav-item" ><Link className="nav-link" activeClassName="active" to="/flights/planes">Lėktuvo priskyrimas</Link></li>
					<li className="nav-item" ><Link className="nav-link" activeClassName="active" to="/flights/staged">Lėktuvų būsenos stebejimas</Link></li>
					<br/>
				</ul>
				</nav>
				{this.props.children}
			</div>
		);
	}
}

function mapStoreToProps(store) {
	return {
		user: store.user
	};
}

export default connect(mapStoreToProps)(FlightWrapper);