import React, { Component } from 'react';
import ReactTable from 'react-table';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import moment from 'moment';
import FlightsActions from '../../../actions/FlightsActions';

class Display extends Component {
	constructor() {
		super();
	}

	componentWillMount() {
		this.props.dispatch(FlightsActions.getAll());
	}

	render() {
		return (
			<div>
				<Link className="btn btn-primary" to="/flights/flights/create">Sukurti skrydį</Link>
				<ReactTable
					data={this.props.flights.flightsInfo}
					columns={[
						{
							Header: 'Maršrutas',
							id: 'route',
							accessor: d => `${d.afcountry} ${d.afname} - ${d.atcountry} ${d.atname}`
						},
						{
							Header: 'Skrydzio pradžios laikas',
							id: 'start',
							accessor: d => moment(d.start).format('LLLL')
						},
						{
							Header: 'Skrydzio pabaigos laikas',
							id: 'end',
							accessor: d => moment(d.end).format('LLLL')
						},
						{
							Header: 'Degalų kiekis (t)',
							accessor: 'fuel_consumtion'

						},
						{
							Header: 'Pelnas (Eur)',
							accessor: 'profit'
						},
						{
							Header: 'Pabaigos laiko zona',
							accessor: 'end_time_zone'
						}
					]}
					defaultPageSize={10}
					className="-striped -highlight"
				/>
			</div>
		);
	}
}

function mapStoreToProps(store) {
	return {
		flights: store.flightsInfo
	};
}

export default connect(mapStoreToProps)(Display);