import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import FlightsActions from '../../../actions/FlightsActions';
import PlaneActions from '../../../actions/PlaneActions';

class AssignCreate extends Component {
	constructor() {
		super();

		this.state = {
			plane_num: ''
		};
	}

	componentWillMount() {
		this.props.dispatch(FlightsActions.getById(this.props.params.id));
		this.props.dispatch(PlaneActions.getAllPlanes());
	}

	render() {
		const item = this.props.flight.editPlane;
		const itemOptions = _.map(this.props.planes.planes, (plane) => (
			<option key={plane.id} value={plane.id}>{`${plane.make} ${plane.model}`}</option>
		));
		return (
			<div>
				<p>Skrydis</p>
				<p>{`${item.afcountry} ${item.afname} - ${item.atcountry} ${item.atname}`}</p>

				<select id="plane_num" onChange={(e) => this.handleChange(e)}>
					{itemOptions}
				</select>

				<button onClick={() => this.onSubmit()}>Išsaugoti</button>
			</div>
		);
	}

	onSubmit() {
		const state = this.state;

		const payload = {
			id: this.props.flight.editPlane.id,
			plane_num: state.plane_num
		};

		this.props.dispatch(FlightsActions.updateFlight(payload));
	}

	handleChange(e) {
		const currentState = this.state;
		currentState[e.target.id] = e.target.value;
		this.setState(currentState);
	}
}

function mapStoreToProps(store) {
	return {
		flight: store.flightsInfo,
		planes: store.planes
	};
}

export default connect(mapStoreToProps)(AssignCreate);