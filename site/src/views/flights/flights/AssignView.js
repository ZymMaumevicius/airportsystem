import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Link} from 'react-router';
import FlightsActions from '../../../actions/FlightsActions';
import moment from 'moment';
import ReactTable from 'react-table';

class AssignView extends Component {
	constructor() {
		super();
	}

	componentWillMount() {
		this.props.dispatch(FlightsActions.getAssignable());
	}

	render() {
		return (
			<div>
				<ReactTable
					data={this.props.flights.assign}
					columns={[
						{
							Header: 'Maršrutas',
							id: 'route',
							accessor: d => `${d.afcountry} ${d.afname} - ${d.atcountry} ${d.atname}`
						},
						{
							Header: 'Skrydzio pradžios laikas',
							id: 'start',
							accessor: d => moment(d.start).format('LLL')
						},
						{
							Header: 'Skrydzio pabaigos laikas',
							id: 'end',
							accessor: d => moment(d.end).format('LLL')
						},
						{
							Header: 'Degalų kiekis (t)',
							accessor: 'fuel_consumtion'

						},
						{
							Header: 'Pelnas (Eur)',
							accessor: 'profit'
						},
						{
							Header: 'Pabaigos laiko zona',
							accessor: 'end_time_zone'
						},{
							Header: '',
							accessor: 'id',
							Cell: row => (
								<Link to={`/flights/assign/${row.value}`}> Priskirti lėktuvą </Link>
							)
						}
					]}
					defaultPageSize={10}
					className="-striped -highlight"
				/>
			</div>
		);
	}
}

function mapStoreToProps(store) {
	return {
		flights: store.flightsInfo
	};
}

export default connect(mapStoreToProps)(AssignView);