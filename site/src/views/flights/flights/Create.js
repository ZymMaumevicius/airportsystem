import _ from 'lodash';

import React, { Component } from 'react';
import { connect } from 'react-redux';

import RouteActions from '../../../actions/RouteActions';
import FlightsActions from '../../../actions/FlightsActions';

class Create extends Component {
	constructor() {
		super();

		this.state = {
			route_num: '',
			start: '',
			end: '',
			fuel_consumtion: '',
			profit: '',
			end_time_zone: ''
		};
	}

	componentWillMount() {
		this.props.dispatch(RouteActions.getRoutesInfo());
	}

	render() {
		const routeOptions = _.map(this.props.routes.routes, (value) => {
			return (<option key={value.id} value={value.id}>{`${value.afcountry} ${value.afname} - ${value.atcountry} ${value.atname}`}</option>);
		});

		return (
			<div className="container">
				<div className="row">
					<div className="col-md-6 col-md-offset-6 text-center">
						<select id="route_num" className="form-control" placeholder="Route"
								onChange={(e) => this.handleChange(e)}>
							{routeOptions}
						</select>
						<input type="date" id="start" className="form-control" placeholder="Start"
							   onChange={(e) => this.handleChange(e)}/>
						<input type="date" id="end" className="form-control" placeholder="End"
							   onChange={(e) => this.handleChange(e)}/>
						<input type="number" id="fuel_consumtion" className="form-control" placeholder="Fuel consumption"
							   onChange={(e) => this.handleChange(e)}/>
						<input type="number" id="profit" className="form-control"
							   placeholder="Profit"
							   onChange={(e) => this.handleChange(e)}/>
						<input type="number" id="end_time_zone" className="form-control" placeholder="End time zone"
							   onChange={(e) => this.handleChange(e)}/>
						{this.state.error && (
							<div className="alert alert-danger" role="alert">{this.state.error}</div>)}
						<button className="btn btn-primary" onClick={() => this.onSubmit()}> GO</button>
					</div>
				</div>
			</div>
		);
	}

	onSubmit() {
		let empty;
		const currentState = this.state;
		_.forOwn(currentState, (value, key) => {
			if (value.length === 0) {
				empty = key;
			}
		});

		if (empty) {
			this.setState({
				error: 'Enter your ' + empty
			});

			return;
		}

		delete currentState.error;

		this.props.dispatch(FlightsActions.saveFilght(currentState));
	}

	handleChange(e) {
		const currentState = this.state;
		currentState[e.target.id] = e.target.value;
		this.setState(currentState);
	}
}

function mapStateToProps(store) {
	return {
		error: store.error,
		routes: store.routes
	};
}

export default connect(mapStateToProps)(Create);