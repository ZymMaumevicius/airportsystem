import React, { Component } from 'react';
import ReactTable from 'react-table';
import {connect} from 'react-redux';
import FlightsActions from '../../../actions/FlightsActions';


let interval = {};

class Display extends Component {
	constructor() {
		super();
	}

	componentWillMount() {
		interval = setInterval(this.updateView.bind(this), 1000);
	}

	componentWillUnmount() {
		clearInterval(interval);
	}

	render() {
		return (
			<div>
				<ReactTable
					data={this.props.flight.statusPlanes}
					columns={[
						{
							Header: 'Skrydžio maršrutas',
							id: 'route',
							accessor: d =>  `${d.afcountry} ${d.afname} - ${d.atcountry} ${d.atname}`

						},
						{
							Header: 'Lektuvo numeris',
							id: 'planeInfo',
							accessor: d => `${d.make} ${d.model} ${d.number}`
						},
						{
							Header: 'Busena',
							accessor: 'stage'
						}
					]}
					defaultPageSize={10}
					className="-striped -highlight"
				/>
			</div>
		);
	}

	updateView() {
		this.props.dispatch(FlightsActions.getStatusInfo());
	}
}

function mapStoreToProps(store) {
	return {
		flight: store.flightsInfo
	};
}

export default connect(mapStoreToProps)(Display);