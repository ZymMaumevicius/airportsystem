import _ from 'lodash';
import React, { Component } from 'react';
import AuthorizationActions from '../../actions/AuthorizationActions';
import ErrorHandlingActions from '../../actions/ErrorHandlingActions';
import { connect } from 'react-redux';

class Login extends Component {
	constructor() {
		super();

		this.state = {
			username: '',
			password: ''
		};
	}

	componentWillUnmount() {
		this.props.dispatch(ErrorHandlingActions.clearErrors());
	}

	render() {
		return (
			<div className="container">
				<div className="row">
					<div className="col-md-6 col-md-offset-6 text-center">
						<input type="text" id="username" className="form-control" placeholder="Full Name"
							   onChange={(e) => this.handleChange(e)}/>
						<input type="password" id="password" className="form-control" placeholder="Password"  onChange={(e) => this.handleChange(e)}/>
						{this.state.error && (<div className="alert alert-danger" role="alert">{this.state.error}</div>)}
						{this.props.error.error && (<div className="alert alert-danger" role="alert">{this.props.error.error}</div>)}

						<button className="btn btn-primary" onClick={() => this.onSubmit()}> GO </button>
					</div>
				</div>
			</div>
		);
	}

	onSubmit() {
		let empty;
		const currentState = this.state;
		_.forOwn(currentState, (value, key) => {
			if (value.length === 0) {
				empty = key;
			}
		});

		if (empty) {
			this.setState({
				name: '',
				password: '',
				error: 'Enter your ' + empty
			});

			return;
		}

		const payload = {
			email: currentState.username,
			password: currentState.password
		};

		this.props.dispatch(AuthorizationActions.login(payload));
	}

	handleChange(e) {
		const currentState = this.state;
		currentState[e.target.id] = e.target.value;
		this.setState(currentState);
	}
}

function mapStateToProps(state) {
	return {
		error: state.error
	};
}

export default connect(mapStateToProps)(Login);
