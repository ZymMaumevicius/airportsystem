import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';

import AuthEnum from '../../../../shared/AuthEnum';
import AuthorizationActions from '../../actions/AuthorizationActions';
import ErrorHandligActions from '../../actions/ErrorHandlingActions';

class Register extends Component {
	constructor() {
		super();

		this.state = {
			name: '',
			surename: '',
			email: '',
			password: '',
			rights: ''
		};
	}

	componentWillUnmount() {
		this.props.dispatch(ErrorHandligActions.clearErrors());
	}

	render() {
		const options = [];
		_.forOwn(AuthEnum, (value, key) => {
			options.push(<option key={key} value={value}>{key}</option>);
		});

		return (
			<div className="container">
				<div className="row">
					<div className="col-md-6 col-md-offset-6 text-center">
						<input type="text" id="name" className="form-control" placeholder="Name"
							   onChange={(e) => this.handleChange(e)}/>
						<input type="text" id="surename" className="form-control" placeholder="Surname"
							   onChange={(e) => this.handleChange(e)}/>
						<input type="text" id="email" className="form-control" placeholder="Email"
							   onChange={(e) => this.handleChange(e)}/>
						<input type="password" id="password" className="form-control" placeholder="Password"
							   onChange={(e) => this.handleChange(e)}/>
						<input type="password" id="password2" className="form-control" placeholder="Repeat password"
							   onChange={(e) => this.handleChange(e)}/>
						<select className="form-control" placeholder="Select rights..." id="rights"
								onChange={(e) => this.handleChange(e)}>
							{options}
						</select>
						{this.state.error && (
							<div className="alert alert-danger" role="alert">{this.state.error}</div>)}
						{this.props.error.error && (
							<div className="alert alert-danger" role="alert">{this.props.error.error}</div>)}
						<button className="btn btn-primary" onClick={() => this.onSubmit()}> GO</button>
					</div>
				</div>
			</div>
		);
	}

	onSubmit() {
		let empty;
		const currentState = this.state;
		_.forOwn(currentState, (value, key) => {
			if (value.length === 0) {
				empty = key;
			}
		});

		if (empty) {
			this.setState({
				name: '',
				password: '',
				error: 'Enter your ' + empty
			});

			return;
		}

		if (currentState.password !== currentState.password2) {
			this.setState({
				name: '',
				password: '',
				error: 'Passwords do not match'
			});

			return;
		}

		const payload = {
			name: currentState.name,
			email: currentState.email,
			password: currentState.password,
			rights: currentState.rights,
			surname: currentState.surename
		};

		this.props.dispatch(AuthorizationActions.register(payload));
	}

	handleChange(e) {
		const currentState = this.state;
		currentState[e.target.id] = e.target.value;
		this.setState(currentState);
	}
}

function mapStateToProps(state) {
	return {
		error: state.error
	};
}


export default connect(mapStateToProps)(Register);