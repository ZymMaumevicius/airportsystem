import React, { Component } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import AuthEnum from '../../../../shared/AuthEnum';
import AuthorizationActions from '../../actions/AuthorizationActions';

class AdminPage extends Component {
	constructor(...args) {
		super(...args);
	}

	componentWillMount() {
		this.props.dispatch(AuthorizationActions.authorize(AuthEnum.Admin));
	}

	render() {
		return (
			<div className="container-fluid">
				<div className="row" style={{marginTop: '30px'}}>
					<div className="col">
						<Link className="btn btn-primary"  style={{width: '100%'}}>Bilietai</Link>
					</div>
					<div className="col">
						<Link className="btn btn-primary" style={{width: '100%'}} to="/flights">Skrydžiai</Link>
					</div>
					<div className="col">
						<Link className="btn btn-primary" style={{width: '100%'}} to="/planes">Lėktuvai</Link>
					</div>
				</div>
			</div>
		);
	}
}

export default connect()(AdminPage);