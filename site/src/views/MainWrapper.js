import React, { Component } from 'react';

class MainWrapper extends Component {
	render() {
		return (
			<div>
				{this.props.children}
			</div>
		);
	}
}

export default MainWrapper;