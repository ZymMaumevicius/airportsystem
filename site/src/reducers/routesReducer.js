import _ from 'lodash';

const defaultState = {
	airports: []
};

export default function (state = defaultState, action) {
	switch (action.type) {
		case 'AIRPORTS_INFO':
			return _.assign({}, state, {airports: action.payload});
		case 'ROUTE_SAVED':
			return _.assign({}, state, {routes: action.payload});
		case 'ROUTES_INFO':
			return _.assign({}, state, {routes: action.payload});
		default:
			return state;
	}
}