import _ from 'lodash';

const defaultState = {
	flightsInfo: [],
	assign: [],
	editPlane: {},
	statusPlanes: []
};

export default function (state = defaultState, action) {
	switch (action.type) {
		case 'FLIGHTS_SAVE':
			return _.assign({}, state, {flightsInfo: action.payload});
		case 'ASSIGN_VIEW':
			return _.assign({}, state, {assign: action.payload});
		case 'STATUS_PLANES':
			return _.assign({}, state, {statusPlanes: action.payload});
		case 'EDIT_PLANE':
			return _.assign({}, state, {editPlane: action.payload});
		default:
			return state;
	}
}