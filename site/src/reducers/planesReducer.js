import _ from 'lodash';

const defaultState = {
    planes: [],
    baggage: [],
    baggageItem: {},
    techPlanes: [],
    planeInfo: {},
    crewList: [],
    crewInfo: {},
    workersAvailable: [],
    crewMembers: []
};

export default function (state = defaultState, action) {
    switch (action.type) {
        case 'PLANE_SAVED':
            return _.assign({}, state, {planes: action.payload});
        case 'PLANES_LIST':
            return _.assign({}, state, {planes: action.payload});
        case 'BAGGAGE_LIST':
            return _.assign({}, state, {baggage: action.payload});
        case 'BAGGAGE_EDIT':
            return _.assign({}, state, {baggageItem: action.payload});
        case 'BAGGAGE_UPDATED':
            return _.assign({}, state, {baggage: action.payload});
        case 'TECH_LIST':
            return _.assign({}, state, {techPlanes: action.payload});
        case 'PLANE_INFO':
            return _.assign({}, state, {planeInfo: action.payload});
        case 'TECH_SAVED':
            return _.assign({}, state, {techPlanes: action.payload});
        case 'CREW_LIST':
            return _.assign({}, state, {crewList: action.payload});
        case 'CREW_INFO':
            return _.assign({}, state, {crewInfo: action.payload});
        case 'CREW_WORKERS_AVAILABLE':
            return _.assign({}, state, {workersAvailable: action.payload});
        case 'CREW_MEMBERS':
            return _.assign({}, state, {crewMembers: action.payload});
        case 'CREW_SAVED':
            return _.assign({}, state, {crewList: action.payload});
        case 'CREW_DELETED':
            return _.assign({}, state, {crewList: action.payload});

        default:
            return state;
    }
}