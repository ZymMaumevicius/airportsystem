import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import userReducer from './userReducer';
import errorReducer from './errorReducer';
import routesReducer from './routesReducer';
import flightsInfoReducer from './flightsInfoReducer';
import planesReducer from './planesReducer';

export default combineReducers({
    routing: routerReducer,
    user: userReducer,
    error: errorReducer,
    routes: routesReducer,
    flightsInfo: flightsInfoReducer,
    planes: planesReducer
});