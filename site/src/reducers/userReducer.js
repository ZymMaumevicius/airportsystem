import _ from 'lodash';
import AuthEnum from '../../../shared/AuthEnum';

const defaultState = {
    username: 'Džiaugias Molis',
    rights: AuthEnum.Tickets
};

export default function (state = defaultState, action) {
    switch (action.type) {
        case 'LOGIN_SUCCESS':
            return _.assign({}, state, action.payload);
        default:
            return state;
    }
}