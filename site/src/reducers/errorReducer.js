import _ from 'lodash';

const defaultState = {
	error: null
};

export default function (state = defaultState, action) {
	switch (action.type) {
		case 'LOGIN_FAILED':
			return _.assign({}, {error: 'Login failed, try again !'});
		case 'REGISTER_FAILED':
			return _.assign({}, {error: 'Register failed'});
		case 'CLEAR_ERROR':
			return {error: null};
		default:
			return state;
	}
}