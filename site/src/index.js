import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route } from 'react-router';

import { store, history } from './store';
import Page from './views/Page';

import Login from './views/login/Login';
import Register from './views/login/Register';

import FlightWrapper from './views/flights/FlightWrapper';
import FlightsDisplay from './views/flights/flights/Display';
import FlightsCreate from './views/flights/flights/Create';
import RoutesCreate from './views/flights/routes/Create';
import RoutesDisplay from './views/flights/routes/Display';
import StageDisplay from './views/flights/stage/Display';
import PlaneWrapper from './views/planes/PlaneWrapper';
import PlanesCreate from './views/planes/planes/Create';
import PlanesDisplay from './views/planes/planes/Display';
import BaggageDisplay from './views/planes/baggage/Display';
import AssignView from './views/flights/flights/AssignView';
import AssignCreate from './views/flights/flights/AssignCreate';
import BaggageEdit from './views/planes/baggage/Edit';
import TechList from './views/planes/tech/Display';
import TechCreate from './views/planes/tech/Create';
import CrewList from './views/planes/crew/List';
import CrewEdit from './views/planes/crew/Edit';
import AdminPage from './views/default/AdminPage';

render(
	<Provider store={store}>
		<Router history={history}>
			<Route path="/" component={Page}>
				<Route path="main" component={Page}/>
				<Route path="login" component={Login}/>
				<Route path="admin" component={AdminPage}/>
				<Route path="register" component={Register}/>
				<Route path="flights" component={FlightWrapper}>
					<Route path="staged" component={StageDisplay}/>
					<Route path="planes" component={AssignView}/>
					<Route path="assign/:id" component={AssignCreate}/>
					<Route path="routes" component={RoutesDisplay}/>
					<Route path="routes/create" component={RoutesCreate}/>
					<Route path="flights" component={FlightsDisplay}/>
					<Route path="flights/create" component={FlightsCreate}/>

				</Route>

				<Route path="planes" component={PlaneWrapper}>
					<Route path="baggage/list" component={BaggageDisplay}/>
					<Route path="baggage/edit/:id" component={BaggageEdit}/>

					<Route path="newplane" component={PlanesCreate}/>
					<Route path="planeslist" component={PlanesDisplay}/>

					<Route path="tech/list" component={TechList}/>
					<Route path="tech/create/:id" component={TechCreate}/>

					<Route path="crew/list" component={CrewList}/>
					<Route path="crew/edit/:id" component={CrewEdit}/>
				</Route>
			</Route>
		</Router>
	</Provider>
	, document.getElementById('root')
);