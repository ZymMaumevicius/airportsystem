import http from '../core/http/http';
import {history} from '../store';

export default {
	saveFilght,
	getAll,
	getAssignable,
	getById,
	updateFlight,
	getStatusInfo
};

function saveFilght(flight) {
	return (dispatch) => {
		return http.post('flights/saveFlight', {flight})
			.then(({data}) => {
				return dispatch({
					type: 'FLIGHTS_SAVE',
					payload: data
				});
			}, (error) => {
				console.log(error);
			});
	};
}

function getAll() {
	return (dispatch) => {
		return http.get('flights/getall')
			.then(({data}) => {
				return dispatch({
					type: 'FLIGHTS_SAVE',
					payload: data
				});
			}, (error) => {
				console.log(error);
			});
	};
}

function getAssignable() {
	return (dispatch) => {
		return http.get('flights/assignable')
			.then(({data}) => {
				return dispatch({
					type: 'ASSIGN_VIEW',
					payload: data
				});
			});
	};
}

function getById(id) {
	return (dispatch) => {
		return http.post('flights/getbyid', { id })
			.then(({ data }) => {
				return dispatch({
					type: 'EDIT_PLANE',
					payload: data
				});
			}, (error) => {
				console.log(error);
			});
	};
}

function updateFlight(payload) {
	return (dispatch) => {
		return http.post('flights/assign', payload)
			.then(({data}) => {

				history.push('/flights/planes');

				return dispatch({
					type: 'ASSIGN_VIEW',
					payload: data
				});
			}, (error) => {
				console.log(error);
			});
	};
}

function getStatusInfo() {
	return (dispatch) => {
		return http.get('flights/status')
			.then(({data}) => {
				dispatch({
					type: 'STATUS_PLANES',
					payload: data
				});
			});
	};
}