import http from '../core/http/http';
import {history} from '../store';

export default {
    createPlane,
    getAllPlanes,
    getPlaneInfo,
    getAllBaggage,
    getBaggage,
    updateBaggage,
    getTechPlanes,
    saveTechData,
    getCrewList,
    getCrew,
    getWorkers,
    getCrewMembers,
    saveCrewInfo,
    deleteCrew
};

// Lektuvai

function createPlane(plane) {
    return (dispatch) => {
        return http.post('/planes/savePlane', {plane})
            .then(({data}) => {
                dispatch({
                    type: 'PLANE_SAVED',
                    payload: data
                });

                history.push('/planes/planeslist');
            }, (error) => {
                console.log(error);
            });
    };
}

function getAllPlanes() {
    return (dispatch) => {
        return http.get('/planes/planelist')
            .then(({data}) => {
                return dispatch({
                    type: 'PLANES_LIST',
                    payload: data
                });
            });
    };
}

// Bagazo organizavimas

function getAllBaggage() {
    return (dispatch) => {
        return http.get('/planes/baggagelist')
            .then(({data}) => {
                return dispatch({
                    type: 'BAGGAGE_LIST',
                    payload: data
                });
            });
    };
}

function getBaggage(id) {
    return ((dispatch) => {
        return http.get('/planes/baggage/edit/' + id)
            .then(({data}) => {
                dispatch({
                    type: 'BAGGAGE_EDIT',
                    payload: data
                });
            });
    });
}

function updateBaggage(baggage) {
    return ((dispatch) => {
        return http.post('/planes/baggage/update', {baggage})
            .then(({data}) => {
                dispatch({
                    type: 'BAGGAGE_UPDATED',
                    payload: data
                });

                history.push('/planes/baggage/list');
            });
    });
}

// Technine apziura

function getTechPlanes() {
    return ((dispatch) => {
        return http.get('planes/tech/list')
            .then(({data}) => {
                dispatch({
                    type: 'TECH_LIST',
                    payload: data
                });
            });
    });
}

function getPlaneInfo(id) {
    return ((dispatch) => {
        return http.get('/planes/getInfo/' + id)
            .then(({data}) => {
                dispatch({
                    type: 'PLANE_INFO',
                    payload: data
                });
            });
    });
}

function saveTechData(tech) {
    return ((dispatch) => {
        return http.post('/planes/tech/save', {tech})
            .then(({data}) => {
                dispatch({
                    type: 'TECH_SAVED',
                    payload: data
                });

                history.push('/planes/tech/list');
            });
    });
}

function getCrewList() {
    return ((dispatch) => {
        http.get('/planes/crew/list')
            .then(({data}) => {
                dispatch({
                    type: 'CREW_LIST',
                    payload: data
                });
            });
    });
}

function getCrew(id) {
    return ((dispatch) => {
        return http.get('/planes/crew/info/' + id)
            .then(({data}) => {
                dispatch({
                    type: 'CREW_INFO',
                    payload: data
                });
            });
    });
}

function getWorkers() {
    return ((dispatch) => {
        return http.get('/planes/crew/workers')
            .then(({data}) => {
                dispatch({
                    type: 'CREW_WORKERS_AVAILABLE',
                    payload: data
                });
            });
    });
}

function getCrewMembers(id) {
    return ((dispatch) => {
        return http.get('/planes/crew/members/' + id)
            .then(({data}) => {
                dispatch({
                    type: 'CREW_MEMBERS',
                    payload: data
                });

            });
    });
}

function saveCrewInfo(crew) {
    return ((dispatch) => {
        return http.post('/planes/crew/save', {crew})
            .then(({data}) => {
                dispatch({
                    type: 'CREW_SAVED',
                    payload: data
                });

                history.push('/planes/crew/list');
            });
    });
}

function deleteCrew(id) {
    return ((dispatch) => {
        return http.get('/planes/crew/delete/' + id)
            .then(({data}) => {
                dispatch({
                    type: 'CREW_DELETED',
                    payload: data
                });

                history.push('/planes/crew/list');
            });
    });
}