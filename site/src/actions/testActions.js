import http from '../core/http/http';

export default {
	testRedux
};

function testRedux() {
	return (dispatch) => {
		return http.get('test')
			.then((result) => {
				dispatch({
					type: 'TEST',
					payload: result
				});


				// return http.get('test')
			});
	};
}
