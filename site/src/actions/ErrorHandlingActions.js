export default {
	clearErrors() {
		return (dipatch) => {
			return dipatch({
				type: 'CLEAR_ERROR',
				payload: {}
			});
		};
	}
};
