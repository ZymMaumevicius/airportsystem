import http from '../core/http/http';
import { history } from './../store';

import { verifyToken } from '../core/objVerifier';
import AuthEnum from '../../../shared/AuthEnum';

export default {
	login(user) {
		return (dispatch) => {
			return http.post('login', user)
				.then((response) => {
					if (response.status === 200) {
						const token = response.data.header;

						const tokenInfo = token.split('.');
						const verifiedObj = verifyToken(atob(tokenInfo[1]), ['email', 'name', 'rights']);
						if (verifiedObj.success) {
							http.defaults.headers.common['Authorization'] = token;
							window.localStorage.setItem('token', token);


							dispatch({
								type: 'LOGIN_SUCCESS',
								payload: verifiedObj.object
							});

							switch (verifiedObj.object.rights) {
								case AuthEnum.Admin:
									history.push('/admin');
									break;
								case AuthEnum.Flights:
									history.push('/flights');
									break;
								case AuthEnum.Planes:
									history.push('/planes');
									break;
								case 1:
								default:
									history.push('/');
							}
						}
					}
				}, () => {

					return dispatch({
						type: 'LOGIN_FAILED',
						payload: {}
					});
				});

		};
	},

	logout() {
		return (dispatch) => {
			window.localStorage.clear();
			http.defaults.headers.common['Authorization'] = undefined;

			return dispatch({
				type: 'LOGOUT',
				payload: {}
			});
		};
	},

	register(payload) {
		return (dispatch) => {
			http.post('register', payload)
				.then(() => {
					history.push('/login');
				}, () => {

					return dispatch({
						type: 'REGISTER_FAILED',
						payload: {}
					});
				});
		};
	},

	authorize(authLevel) {
		return (dispatch) => {
			const token = window.localStorage.getItem('token');
			if (token) {
				const tokenInfo = token.split('.');
				const verifiedObj = verifyToken(atob(tokenInfo[1]), ['email', 'name', 'rights']);
				if (verifiedObj.success) {
					http.defaults.headers.common['Authorization'] = token;
					if (verifiedObj.object.rights !== AuthEnum.Admin && verifiedObj.object.rights !== authLevel) {
						history.push('login');
					}

					return dispatch({
						type: 'LOGIN_SUCCESS',
						payload: verifiedObj.object
					});

				}
			}

			history.push('login');
		};
	}
};
