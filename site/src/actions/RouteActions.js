import http from '../core/http/http';
import {history} from '../store';

export default {
	getAirports,
	createRoute,
	getRoutesInfo
};

function getAirports() {
	return (dispatch) => {
		return http.get('routes/getInfo')
			.then(({data}) => {
				dispatch({
					type: 'AIRPORTS_INFO',
					payload: data
				});
			}, (error) => {
				console.log(error);
			});
	};
}

function createRoute(route) {
	return (dispatch) => {
		return http.post('routes/save', {route})
			.then(({data}) => {
				dispatch({
					type: 'ROUTE_SAVED',
					payload: data
				});

				dispatch({
					type: 'CLEAR_ERROR',
					payload: {}
				});

				history.push('/flights/routes');
			}, (error) => {
				console.log(error);
			});

	};
}

function getRoutesInfo() {
	return (dispatch) => {
		return http.get('routes/info')
			.then(({data}) => {
				return dispatch({
					type: 'ROUTES_INFO',
					payload: data
				});
			});
	};
}