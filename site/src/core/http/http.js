import config from '../../../config/config.json';
import axios from 'axios';

const http = axios.create({
	baseURL: config.serverUrl,
	timeout: 100000
});

http.defaults.headers.common['Authorization'] = window.localStorage['token'];

http.setSessionToken = (token) => {
	http.defaults.headers.common['Authorization'] = token;
};

export default http;