import _ from 'lodash';


export function verifyToken(obj, params = []) {
	const parsedObj = JSON.parse(obj);
	if (_.isNil(parsedObj)) {
		return { success: false };
	}

	const expDate = new Date(1970, 1, 1);
	expDate.setSeconds(parsedObj.exp);

	if (expDate < new Date()) {
		return { success: false };
	}

	const agg = {};

	_.forEach(params, (param) => {
		agg[param] = parsedObj.user[param];
	});

	return { success: true, object: agg };
}