
drop sequence if exists user_id_seq;
drop sequence if exists airport_id_seq;
drop sequence if exists route_id_seq;
drop sequence if exists flight_status_id_seq;
drop sequence if exists flight_id_seq;

CREATE SEQUENCE user_id_seq;

CREATE TABLE public.user (
  id INTEGER NOT NULL PRIMARY KEY DEFAULT NEXTVAL('user_id_seq'),
  name CHARACTER VARYING(255),
  surname CHARACTER VARYING(255),
  email CHARACTER VARYING(255),
  rights INTEGER,
  password CHARACTER VARYING(255),
  "createdOn" TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now(),
  "deletedOn" TIMESTAMP WITHOUT TIME ZONE,
  "updatedOn" TIMESTAMP WITHOUT TIME ZONE,
  deleted boolean NOT NULL DEFAULT false
);

CREATE SEQUENCE airport_id_seq;

CREATE TABLE public.airport (
    id INTEGER NOT NULL PRIMARY KEY DEFAULT NEXTVAL('airport_id_seq'),
    name CHARACTER VARYING(32),
    city CHARACTER VARYING(32),
    country CHARACTER VARYING(30),
    latitude decimal,
    longitude decimal,
    track_length decimal,
    track_primer CHARACTER VARYING(32),
    track_comments CHARACTER VARYING(200),
    timezone integer,
    "createdOn" TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now(),
    "deletedOn" TIMESTAMP WITHOUT TIME ZONE,
    "updatedOn" TIMESTAMP WITHOUT TIME ZONE
);

CREATE SEQUENCE route_id_seq;

CREATE TABLE public.route (
    id integer not null primary key default nextval('route_id_seq'),
    airport_from_id integer,
    airport_to_id integer,
    valid_from Timestamp without time zone,
    valid_to Timestamp without time zone,
    planned_traffic decimal,
    fuel_consumption_coef decimal,
    distance decimal,
    airtime_duration decimal,
    "createdOn" TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now(),
    "deletedOn" TIMESTAMP WITHOUT TIME ZONE,
    "updatedOn" TIMESTAMP WITHOUT TIME ZONE,

    CONSTRAINT airportTo FOREIGN KEY (airport_to_id) REFERENCES public.airport (id),
    CONSTRAINT airportRFrom FOREIGN KEY (airport_from_id) REFERENCES public.airport (id)
);

CREATE SEQUENCE flight_status_id_seq;

CREATE TABLE public.flight_status (
    id integer not null primary key default nextval('flight_status_id_seq'),
    name character varying(40)
);

INSERT INTO public.flight_status (name)
VALUES
('Laukiama vartų'), ('Vartai atidaryti'), ('Vartai užsidaro'), ('Vartai uždaryti'),
('Vyksta į pakilimo taką'), ('Laukia tako'), ('Kyla'), ('Skrenda'), ('Leidžiasi'), ('Nusileido');

CREATE SEQUENCE flight_id_seq;

CREATE TABLE public.flight (
    id integer not null primary key default nextval('flight_id_seq'),
    "start" TIMESTAMP WITHOUT TIME ZONE,
    "end" TIMESTAMP WITHOUT TIME ZONE,
    plane_num integer,
    route_num integer,
    flight_status integer,
    fuel_consumtion decimal,
    profit decimal,
    end_time_zone int,
    "createdOn" TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now(),
    "deletedOn" TIMESTAMP WITHOUT TIME ZONE,
    "updatedOn" TIMESTAMP WITHOUT TIME ZONE,

    CONSTRAINT route_number FOREIGN KEY (route_num)
    REFERENCES public.route (id),

    CONSTRAINT flight_status_fk FOREIGN KEY (flight_status)
    REFERENCES public.flight_status (id)
);
