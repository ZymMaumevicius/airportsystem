create sequence worker_type_seq;

create table public.worker_type (
    id integer not null primary key default nextval('worker_type_seq'),
    name character varying(40)
);

create sequence worker_id_seq;

create table public.worker (
    id integer not null primary key default nextval('worker_id_seq'),
    first_name character varying(20),
    last_name character varying(40),
    birth_date timestamp without time zone not null,
    gender boolean not null,
    registered_on timestamp without time zone not null default now(),
    user_type_id integer not null,

    constraint worker_worker_type foreign key (user_type_id) references public.worker(id)
);

alter table public.plane_crew add column worker_id integer not null;
alter table public.plane_crew add constraint crew_worker_id foreign key (worker_id) references public.worker(id);