alter table public.worker drop constraint worker_worker_type;
alter table public.worker add constraint worker_worker_type foreign key (user_type_id) references public.worker_type(id);