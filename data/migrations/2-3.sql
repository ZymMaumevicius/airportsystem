CREATE SEQUENCE plane_id_seq;

CREATE TABLE public.plane (
    id INTEGER NOT NULL PRIMARY KEY DEFAULT NEXTVAL('plane_id_seq'),
    "number" CHARACTER VARYING(20),
    model CHARACTER VARYING(20),
    make CHARACTER VARYING(32),
    weight DECIMAL NOT NULL,
    lifting_power DECIMAL NOT NULL,
    baggage_volume DECIMAL NOT NULL,
    business_seats INTEGER NOT NULL,
    economy_seats INTEGER NOT NULL,
    pilots INTEGER NOT NULL,
    stewards INTEGER NOT NULL,
    toilets INTEGER NOT NULL,
    "createdOn" TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now(),
    "deletedOn" TIMESTAMP WITHOUT TIME ZONE,
    "updatedOn" TIMESTAMP WITHOUT TIME ZONE,
    deleted boolean NOT NULL DEFAULT false
);

CREATE SEQUENCE technical_checkup_id_seq;

CREATE TABLE public.technical_checkup (
    id INTEGER NOT NULL PRIMARY KEY DEFAULT NEXTVAL('technical_checkup_id_seq'),
    number character varying(32),
    "date" TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    check_is_passed boolean NOT NULL,
    comments CHARACTER VARYING(250),
    chassis boolean NOT NULL,
    flaps boolean NOT NULL,
    steering_mechanism boolean NOT NULL,
    landing_gear boolean NOT NULL,
    engines boolean NOT NULL,
    fuel_system boolean NOT NULL,
    communications boolean NOT NULL,
    lights boolean NOT NULL,
    brakes boolean NOT NULL,
    plane_id INTEGER NOT NULL,
    "createdOn" TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now(),
    "deletedOn" TIMESTAMP WITHOUT TIME ZONE,
    "updatedOn" TIMESTAMP WITHOUT TIME ZONE,
    deleted boolean NOT NULL DEFAULT false,

    CONSTRAINT plane_technical_checkup FOREIGN KEY (plane_id) REFERENCES public.plane(id)
);

create sequence plane_baggage_id_seq;

create table public.plane_baggage (
    id integer not null primary key default nextval('plane_baggage_id_seq'),
    number character varying(40),
    weight decimal not null,
    width decimal not null,
    height decimal not null,
    "length" decimal not null,
    plane_id integer,
     "createdOn" TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now(),
     "deletedOn" TIMESTAMP WITHOUT TIME ZONE,
     "updatedOn" TIMESTAMP WITHOUT TIME ZONE,
     deleted boolean NOT NULL DEFAULT false,

     constraint plane_baggage_plane foreign key (plane_id) references public.plane(id)
);

create sequence plane_crew_id_seq;

create table public.plane_crew (
    id integer not null primary key default nextval('plane_crew_id_seq'),
    number character varying(40),
    valid_from timestamp without time zone not null,
    valid_to timestamp without time zone not null,
    is_disabled boolean not null,
    plane_id integer,
    "createdOn" TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now(),
    "deletedOn" TIMESTAMP WITHOUT TIME ZONE,
    "updatedOn" TIMESTAMP WITHOUT TIME ZONE,
    deleted boolean NOT NULL DEFAULT false,

    constraint plane_crew_plane foreign key (plane_id) references public.plane(id)
);

create sequence crew_members_id_seq;

create table public.crew_members (
    id integer not null primary key default nextval('crew_members_id_seq'),
    user_id integer not null,
    crew_id integer not null,

    constraint crew_members_user foreign key (user_id) references public."user"(id),
    constraint crew_members_crew foreign key (crew_id) references public.crew_members(id)
);