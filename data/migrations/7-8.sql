alter table crew_members drop constraint crew_members_crew;
alter table crew_members add constraint crew_members_crew foreign key (crew_id) references plane_crew(id);