alter table public.plane_crew drop constraint crew_worker_id;
alter table public.plane_crew drop column worker_id;

alter table public.crew_members drop constraint crew_members_user;
alter table public.crew_members drop column user_id;

alter table public.crew_members add column worker_id integer not null;
alter table public.crew_members add constraint crew_members_worker foreign key (worker_id) references public.worker(id);

insert into worker_type (name) values ('Pilotas'), ('Stiuardas');

insert into worker (first_name, last_name, birth_date, gender, user_type_id) values
	('Petras', 'Jonaitis', '1980-10-15', true, 1),
	('Jonas', 'Antanaitis', '1974-08-17', true, 1),
	('Alvydas', 'Romka', '1975-06-29', true, 2),
	('Rasa', 'Vytautienė', '1968-07-21', true, 2),
	('Jurgis', 'Valiulis', '1989-03-14', true, 2),
	('Rimas', 'Visgirda', '1976-07-14', true, 2),
	('Domantas', 'Dimaitis', '1989-06-23', true, 2),
	('Jolanta', 'Jonaitytė', '1987-08-14', true, 2),
	('Vilma', 'Mačiulienė', '1985-06-28', true, 1),
	('Edgaras', 'Palukaitis', '1983-01-29', true, 2),
	('Tomas', 'Tamutis', '1982-05-10', true, 2);